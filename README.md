<!--
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-License-Identifier: Apache-2.0
-->
# openDesk Element Helm Charts

Helm charts for deployment Element including Synapse.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Kubernetes 1.21+
- Helm 3.0.0+
- Optional: PV provisioner support in the underlying infrastructure

## Documentation

The documentation is placed in the README of each helm chart:

- [opendesk-element](charts/opendesk-element)
- [opendesk-matrix-user-verification-service](charts/opendesk-matrix-user-verification-service)
- [opendesk-synapse](charts/opendesk-synapse)
- [opendesk-synapse-admin](charts/opendesk-synapse-admin)
- [opendesk-synapse-adminbot-web](charts/opendesk-synapse-adminbot-web)
- [opendesk-synapse-create-account](charts/opendesk-synapse-create-account)
- [opendesk-synapse-groupsync](charts/opendesk-synapse-groupsync)
- [opendesk-synapse-pipe](charts/opendesk-synapse-pipe)
- [opendesk-synapse-web](charts/opendesk-synapse-web)
- [opendesk-well-known](charts/opendesk-well-known)

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright © 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
