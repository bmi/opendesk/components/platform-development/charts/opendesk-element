<!--
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-License-Identifier: Apache-2.0
-->
# opendesk-element-umbrella

Deploy all Element Components

## Installing the Chart

To install the chart with the release name `my-release`, you have two options:

### Install via Repository
```console
helm repo add opendesk-element https://gitlab.opencode.de/api/v4/projects/2291/packages/helm/stable
helm install my-release --version 6.1.0 opendesk-element/opendesk-element-umbrella
```

### Install via OCI Registry
```console
helm repo add opendesk-element oci://registry.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element
helm install my-release --version 6.1.0 opendesk-element/opendesk-element-umbrella
```

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| file://../opendesk-element | opendesk-element | x.x.x |
| file://../opendesk-matrix-user-verification-service | opendesk-matrix-user-verification-service | x.x.x |
| file://../opendesk-synapse-admin | opendesk-synapse-admin | x.x.x |
| file://../opendesk-synapse-adminbot-web | opendesk-synapse-adminbot-web | x.x.x |
| file://../opendesk-synapse-create-account | opendesk-synapse-adminbot-bootstrap(opendesk-synapse-create-account) | x.x.x |
| file://../opendesk-synapse-create-account | opendesk-matrix-user-verification-service-bootstrap(opendesk-synapse-create-account) | x.x.x |
| file://../opendesk-synapse-create-account | matrix-neodatefix-bot-bootstrap(opendesk-synapse-create-account) | x.x.x |
| file://../opendesk-synapse-create-account | opendesk-synapse-auditbot-bootstrap(opendesk-synapse-create-account) | x.x.x |
| file://../opendesk-synapse-groupsync | opendesk-synapse-groupsync | x.x.x |
| file://../opendesk-synapse-pipe | opendesk-synapse-adminbot-pipe(opendesk-synapse-pipe) | x.x.x |
| file://../opendesk-synapse-pipe | opendesk-synapse-auditbot-pipe(opendesk-synapse-pipe) | x.x.x |
| file://../opendesk-synapse-web | opendesk-synapse-web | x.x.x |
| file://../opendesk-synapse | opendesk-synapse | x.x.x |
| file://../opendesk-well-known | opendesk-well-known | x.x.x |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| global.domain | string | `"example.com"` | The Top-Level-Domain (TLD) name which is used in f.e. in Ingress component. |
| global.hosts.adminBot | string | `"adminbot"` | Subdomain for adminbot, results in "https://{{ adminbot }}.{{ domain}}" |
| global.hosts.element | string | `"element"` | Subdomain for element, results in "https://{{ element }}.{{ domain }}". |
| global.hosts.synapse | string | `"synapse"` | Subdomain for synapse, results in "https://{{ synapse }}.{{ domain }}". |
| global.hosts.synapseAdmin | string | `"synapse-admin"` | Subdomain for synapse admin, results in "https://{{ synapse-admin }}.{{ domain}}" |
| global.imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| global.registry | string | `"registry.opencode.de"` | Container registry address. |
| matrix-neodatefix-bot-bootstrap | object | `{}` | All available config options can be found in the subchart opendesk-synapse-create-account. |
| opendesk-element | object | `{}` | All available config options can be found in the subchart opendesk-element. |
| opendesk-matrix-user-verification-service | object | `{}` | All available config options can be found in the subchart opendesk-matrix-user-verification-service. |
| opendesk-matrix-user-verification-service-bootstrap | object | `{}` | All available config options can be found in the subchart opendesk-synapse-create-account. |
| opendesk-synapse | object | `{}` | All available config options can be found in the subchart opendesk-synapse. |
| opendesk-synapse-admin | object | `{}` | All available config options can be found in the subchart opendesk-synapse-admin. |
| opendesk-synapse-adminbot-bootstrap | object | `{}` | All available config options can be found in the subchart opendesk-synapse-create-account. |
| opendesk-synapse-adminbot-pipe | object | `{}` | All available config options can be found in the subchart opendesk-synapse-pipe. |
| opendesk-synapse-adminbot-web | object | `{}` | All available config options can be found in the subchart opendesk-synapse-adminbot-web. |
| opendesk-synapse-auditbot-bootstrap | object | `{}` | All available config options can be found in the subchart opendesk-synapse-create-account. |
| opendesk-synapse-auditbot-pipe | object | `{}` | All available config options can be found in the subchart opendesk-synapse-pipe. |
| opendesk-synapse-groupsync | object | `{}` | All available config options can be found in the subchart opendesk-synapse-groupsync. |
| opendesk-synapse-web | object | `{}` | All available config options can be found in the subchart opendesk-synapse-web. |
| opendesk-well-known | object | `{}` | All available config options can be found in the subchart opendesk-well-known. |
| tags.enterprise | bool | `false` | Whether to deploy enterprise components. |

## Uninstalling the Chart

To install the release with name `my-release`:

```bash
helm uninstall my-release
```

## Signing

Helm charts are signed with helm native signing method.

You can verify the chart against [the public GPG key](../../files/gpg-pubkeys/opendesk.gpg).

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright (C) 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
