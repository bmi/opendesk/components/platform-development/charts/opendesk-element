{{/*
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
*/}}
---
apiVersion: {{ include "common.capabilities.cronjob.apiVersion" . | quote }}
kind: "Job"
metadata:
  name: {{ include "common.names.fullname" . | quote }}
  labels:
    {{- include "common.labels.standard" . | nindent 4 }}
  annotations:
    "helm.sh/hook": "pre-delete"
    "helm.sh/hook-delete-policy": before-hook-creation,hook-succeeded
    "argocd.argoproj.io/hook": "Skip"
spec:
  {{- if .Values.cleanup.deletePodsOnSuccess }}
  ttlSecondsAfterFinished: {{ .Values.cleanup.deletePodsOnSuccessTimeout }}
  {{- end }}
  template:
    spec:
      {{- if or .Values.global.imagePullSecrets .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- range .Values.global.imagePullSecrets }}
        - name: {{ . | quote }}
        {{- end }}
        {{- range .Values.imagePullSecrets }}
        - name: {{ . | quote }}
        {{- end }}
      {{- end }}
      serviceAccountName: {{ include "common.names.fullname" . | quote }}
      containers:
        - name: {{ .Chart.Name | quote }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ coalesce .Values.image.registry .Values.global.registry }}/{{ .Values.image.repository }}{{ if .Values.image.digest }}@{{ .Values.image.digest }}{{ else }}:{{ .Values.image.tag }}{{ end }}"
          imagePullPolicy: {{ .Values.image.imagePullPolicy | quote }}
          {{- with .Values.resources }}
          resources:
            {{ toYaml . | nindent 12 | trim }}
          {{- end }}
          tty: true
          env:
            - name: NAMESPACE
              value: {{ .Release.Namespace | quote }}
            - name: SECRET_NAME
              value: {{ .Values.configuration.secretName | quote }}
            {{- if .Values.configuration.pipeConfig.enabled }}
            - name: PC_SECRET_NAME
              value: {{ .Values.configuration.pipeConfig.secretName | quote }}
            {{- end }}
          command:
            - "/bin/bash"
            - "-c"
            - |
              kubectl delete secret --namespace="${NAMESPACE}" ${SECRET_NAME} --ignore-not-found
              {{- if .Values.configuration.pipeConfig.enabled }}
              kubectl delete secret --namespace="${NAMESPACE}" ${PC_SECRET_NAME} --ignore-not-found
              {{- end }}
      restartPolicy: Never
  backoffLimit: 4
...
