<!--
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# opendesk-synapse-create-account

This helm chart contains a bootstrap job for the setup of a technical admin user in a Synapse

## Installing the Chart

To install the chart with the release name `my-release`:

```console
helm repo add opendesk-element https://gitlab.opencode.de/api/v4/projects/2291/packages/helm/stable
helm install my-release opendesk-element/opendesk-synapse-create-account
```

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| oci://registry.opencode.de/bmi/opendesk/components/external/charts/bitnami-charts | common | ^2.x.x |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| cleanup.deletePodsOnSuccess | bool | `true` | Keep Pods/Job logs after successful run. |
| cleanup.deletePodsOnSuccessTimeout | int | `3600` | When deletePodsOnSuccess is enabled, the pod will be deleted after configured seconds. |
| configuration.deviceId | string | `"DEFAULT"` | device id of the session that should be used |
| configuration.password | string | `""` | password of the user that should be created |
| configuration.pipeConfig.asToken | string | `""` | application service token |
| configuration.pipeConfig.backupPassphrase | string | `""` | backup passphrase |
| configuration.pipeConfig.enabled | bool | `false` | wether to create a secret for the adminbot/auditbot pipconfig |
| configuration.pipeConfig.homeserverName | string | `""` | Name of synapse server. As default the name of synapse will be used. |
| configuration.pipeConfig.hsToken | string | `""` | homeserver token |
| configuration.pipeConfig.secretName | string | `""` | name of the secret that will be created for the pipe config |
| configuration.pipeConfig.serviceUrl | string | `""` | service url |
| configuration.pipeConfig.type | string | `"admin"` | type of pipeconfig (only "admin" or "audit" are supported) |
| configuration.pod | string | `"opendesk-synapse-0"` | name of the pod where the synapse is running |
| configuration.secretName | string | `""` | name of the secret that will be created for the user |
| configuration.username | string | `""` | name of the user that should be created |
| fullnameOverride | string | `""` | Provide a name to substitute for the full names of resources. |
| global.domain | string | `"example.com"` | The Top-Level-Domain (TLD) name which is used in f.e. in Ingress component. |
| global.hosts.jitsi | string | `"jitsi"` | Subdomain for jitsi, results in "https://{{ jitsi }}.{{ domain }}". |
| global.hosts.synapse | string | `"synapse"` | Subdomain for synapse, results in "https://{{ synapse }}.{{ domain }}". |
| global.hosts.synapseFederation | string | `"synapse-federation"` | Subdomain for synapse federation, results in "https://{{ synapseFederation }}.{{ domain }}". |
| global.imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| global.registry | string | `"docker.io"` | Container registry address. |
| image.digest | string | `""` | use digest instead of image tag, has precedence over tag! |
| image.imagePullPolicy | string | `"IfNotPresent"` | Define an ImagePullPolicy.  Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy  "IfNotPresent" => The image is pulled only if it is not already present locally. "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to             resolve the name to an image digest. If the kubelet has a container image with that exact digest cached             locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the resolved             digest, and uses that image to launch the container. "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the            kubelet attempts to start the container; otherwise, startup fails  |
| image.registry | string | `"docker.io"` | Container registry address. This setting has higher precedence than global.registry. |
| image.repository | string | `"alpine/k8s"` | Container repository string. |
| image.tag | string | `"1.27.4"` | Overrides the image tag whose default is the chart appVersion. Has no effect if digest is used! |
| imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| nameOverride | string | `""` | String to partially override release name. |
| resources.limits.cpu | int | `1` | The max amount of CPUs to consume. |
| resources.limits.memory | string | `"1Gi"` | The max amount of RAM to consume. |
| resources.requests.cpu | string | `"500m"` | The amount of CPUs which has to be available on the scheduled node. |
| resources.requests.memory | string | `"256Mi"` | The amount of RAM which has to be available on the scheduled node. |
| serviceAccount.annotations | object | `{}` | Additional custom annotations for the ServiceAccount. |

## Uninstalling the Chart

To install the release with name `my-release`:

```bash
helm uninstall my-release
```

## Signing

### Chart

Helm charts are signed with helm native signing method. You can verify the charts against this GPG key:

```

```

### Images

Container images are signed via [cosign](https://github.com/sigstore/cosign) and can be verified with:

```

```

```
cosign verify --key cosign.pub --insecure-ignore-tlog <image>
```

## License
This project uses the following license: Apache-2.0

## Copyright
Copyright (C) 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
