# SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

until kubectl exec --namespace="${NAMESPACE}" ${SYNAPSE_POD} -c synapse \
-- bash -c 'curl -sf http://localhost:8008/health';
do
  echo "waiting for container to be alive..."
  sleep 1
done

ACCESS_TOKEN=$(kubectl exec --namespace="${NAMESPACE}" ${SYNAPSE_POD} -c synapse \
-- bash -c "
echo \"create the user ${MATRIX_USERNAME} if not exists\"
register_new_matrix_user -u ${MATRIX_USERNAME} -a -c /config/homeserver.yaml -p ${MATRIX_PASSWORD} || true

RESULT=''
ACCESS_TOKEN=''
MATRIX_USER_ID=''

create_user() {
    RESULT=\$(curl -X POST -d '{\"type\":\"m.login.password\", \"user\":\"$MATRIX_USERNAME\", \"password\":\"$MATRIX_PASSWORD\", \"device_id\":\"$DEVICE_ID\"}' http://localhost:8008/_matrix/client/v3/login)
    ACCESS_TOKEN=\$(echo \$RESULT | sed -n 's/.*\"access_token\":\"\([^\"]*\)\".*/\1/p')
    MATRIX_USER_ID=\$(echo \$RESULT | sed -n 's/.*\"user_id\":\"\([^\"]*\)\".*/\1/p')

    if [ -z "\${ACCESS_TOKEN}" ]
    then
        return 1
    else
        return 0
    fi
}

echo \"login as user ${MATRIX_USERNAME}\"
until create_user
do
    echo \$RESULT
    echo 'waiting for user to login...'
    sleep 5
done

echo \"disable rate limiting for user ${MATRIX_USERNAME}\"
until curl -X POST -f -d '{\"messages_per_second\": 0, \"burst_count\": 0}' -H \"Authorization: Bearer \$ACCESS_TOKEN\" -H 'Content-Type: application/json' http://localhost:8008/_synapse/admin/v1/users/\$MATRIX_USER_ID/override_ratelimit
do
    echo 'waiting for rate limit to be set...'
    sleep 5
done

echo "\n"
echo \$ACCESS_TOKEN
" | tee /dev/tty | tail -1)

kubectl delete secret --namespace="${NAMESPACE}" ${SECRET_NAME} --ignore-not-found
kubectl create secret --namespace="${NAMESPACE}" generic ${SECRET_NAME} --from-literal=access_token=$ACCESS_TOKEN

{{- if .Values.configuration.pipeConfig.enabled }}

kubectl delete secret --namespace="${NAMESPACE}" ${PC_SECRET_NAME} --ignore-not-found

mkdir /tmp/secret
cat > /tmp/secret/config.yml <<- EOF
{{ tpl (.Files.Get (printf "files/%s-pipe.config" .Values.configuration.pipeConfig.type) ) . }}
EOF

kubectl create secret --namespace="${NAMESPACE}" generic ${PC_SECRET_NAME} --from-file=/tmp/secret

{{- end }}
