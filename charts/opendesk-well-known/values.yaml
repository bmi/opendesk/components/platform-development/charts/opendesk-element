# SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0
---
# The global properties are used to configure multiple charts at once.
global:
  # -- The Top-Level-Domain (TLD) name which is used in f.e. in Ingress component.
  domain: "example.com"

  # Define the Subdomain for components used in f.e. in Ingress component.
  hosts:
    # -- Subdomain for synapse, results in "https://{{ synapse }}.{{ domain }}".
    synapse: "synapse"
    # -- Subdomain for synapse federation, results in "https://{{ synapseFederation }}.{{ domain }}".
    synapseFederation: "synapse-federation"
    # -- Subdomain for jitsi, results in "https://{{ jitsi }}.{{ domain }}".
    jitsi: "jitsi"

  # -- Container registry address.
  registry: "docker.io"

  # -- Credentials to fetch images from private registry
  # Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
  #
  # imagePullSecrets:
  #   - "docker-registry"
  #
  imagePullSecrets: []

# -- Affinity for pod assignment
# Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity
# Note: podAffinityPreset, podAntiAffinityPreset, and  nodeAffinityPreset will be ignored when it's set
affinity: {}

# -- Additional custom annotations to add to all deployed objects.
commonAnnotations: {}

# -- Additional custom labels to add to all deployed objects.
commonLabels: {}

# Well Known specific configuration
configuration:
  # Settings regarding homeserver for connection.
  homeserver:
    # -- URL of matrix deployment.
    baseUrl: ""

    # -- Name of matrix server.
    serverName: ""

  # Settings for the end to end encryption that are provided to Element.
  e2ee:
    # -- Disable all encryption settings in Element so users can't configure encrypted rooms.
    # Ensure that is aligned with `configuration.endToEndEncryption` of the `opendesk-element` deployment.
    forceDisable: false

# Security Context.
# Ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
containerSecurityContext:
  # -- Enable container privileged escalation.
  allowPrivilegeEscalation: false

  # -- Additional capabilities.
  capabilities:
    drop:
      - "ALL"

  # -- Enable security context.
  enabled: true

  # -- Run container in privileged mode.
  privileged: false

  # -- Mounts the container's root filesystem as read-only.
  readOnlyRootFilesystem: true

  # -- Process group id.
  runAsGroup: 101

  # -- Run container as user.
  runAsNonRoot: true

  # -- Process user id.
  runAsUser: 101

  # Set Seccomp profile.
  seccompProfile:
    # -- Disallow custom Seccomp profile by setting it to RuntimeDefault.
    type: "RuntimeDefault"

# -- Array with extra environment variables to add to containers.
#
# extraEnvVars:
#   - name: FOO
#     value: "bar"
#
extraEnvVars: []

# -- Optionally specify extra list of additional volumes
extraVolumes: []

# -- Optionally specify extra list of additional volumeMounts.
extraVolumeMounts: []

# -- Provide a name to substitute for the full names of resources.
fullnameOverride: ""

# Container image section.
image:
  # -- Container registry address. This setting has higher precedence than global.registry.
  registry: ""

  # -- Container repository string.
  repository: "library/nginx"

  # -- Define an ImagePullPolicy.
  #
  # Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy
  #
  # "IfNotPresent" => The image is pulled only if it is not already present locally.
  # "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to
  #             resolve the name to an image digest. If the kubelet has a container image with that exact digest cached
  #             locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the resolved
  #             digest, and uses that image to launch the container.
  # "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the
  #            kubelet attempts to start the container; otherwise, startup fails
  #
  imagePullPolicy: "IfNotPresent"

  # -- Define image tag.
  tag: "1.23"

# -- Credentials to fetch images from private registry
# Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
#
# imagePullSecrets:
#   - "docker-registry"
#
imagePullSecrets: []

# Define and create Kubernetes Ingress.
#
# Ref.: https://kubernetes.io/docs/concepts/services-networking/ingress/
ingress:
  # -- Enable create of Ingress.
  enabled: true

  # -- Define the Fully Qualified Domain Name (FQDN) where UCS Portal should be reachable. This setting has higher
  # precedence than global.registry.
  host: ""

  # -- Define the Ingress path.
  path: "/.well-known/matrix"

  # -- Each path in an Ingress is required to have a corresponding path type. Paths that do not include an explicit
  # pathType will fail validation. There are three supported path types:
  #
  # "ImplementationSpecific" => With this path type, matching is up to the IngressClass. Implementations can treat this
  #                             as a separate pathType or treat it identically to Prefix or Exact path types.
  # "Exact" => Matches the URL path exactly and with case sensitivity.
  # "Prefix" => Matches based on a URL path prefix split by /.
  #
  # Ref.: https://kubernetes.io/docs/concepts/services-networking/ingress/#path-types
  #
  pathType: "Prefix"


  # -- The Ingress controller class name.
  ingressClassName: "nginx"

  # -- Define custom ingress annotations.
  #
  # annotations:
  #   nginx.ingress.kubernetes.io/rewrite-target: /
  #
  annotations: {}

  # -- Secure an Ingress by specifying a Secret that contains a TLS private key and certificate.
  #
  # Ref.: https://kubernetes.io/docs/concepts/services-networking/ingress/#tls
  #
  tls:
    # -- Enable TLS/SSL/HTTPS for Ingress.
    #
    enabled: true

    # -- The name of the kubernetes secret which contains a TLS private key and certificate.
    # Hint: This secret is not created by this chart and must be provided.
    secretName: ""


# -- Lifecycle to automate configuration before or after startup
lifecycleHooks: {}

# -- String to partially override release name.
nameOverride: ""

# -- Node labels for pod assignment
# Ref: https://kubernetes.io/docs/user-guide/node-selection/
nodeSelector: {}

#  Configure extra options for containers probes.
livenessProbe:
  # -- Enables kubernetes LivenessProbe.
  enabled: true
  # -- Number of failed executions until container is terminated.
  failureThreshold: 3
  # -- Delay after container start until LivenessProbe is executed.
  initialDelaySeconds: 10
  # -- Time between probe executions.
  periodSeconds: 10
  # -- Number of successful executions after failed ones until container is marked healthy.
  successThreshold: 1
  # -- Timeout for command return.
  timeoutSeconds: 5

# Database persistence settings.
persistence:
  # Temporary directory.
  temporaryDirectory:
    # -- Size limit of emptyDir volume.
    size: "50Mi"

# -- Pod Annotations.
# Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/
podAnnotations: {}

# -- Pod Labels.
# Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/
podLabels: {}

# Pod Security Context.
# Ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
podSecurityContext:
  # -- Enable security context.
  enabled: true

  # -- If specified, all processes of the container are also part of the supplementary group
  fsGroup: 101

#  Configure extra options for containers probes.
readinessProbe:
  # -- Enables kubernetes ReadinessProbe.
  enabled: true
  # -- Delay after container start until ReadinessProbe is executed.
  initialDelaySeconds: 20
  # -- Number of failed executions until container is terminated.
  failureThreshold: 3
  # -- Time between probe executions.
  periodSeconds: 10
  # -- Number of successful executions after failed ones until container is marked healthy.
  successThreshold: 1
  # -- Timeout for command return.
  timeoutSeconds: 5

# -- Set the amount of replicas of deployment.
replicaCount: 2

# Configure resource requests and limits.
#
# http://kubernetes.io/docs/user-guide/compute-resources/
#
resources:
  limits:
    # -- The max amount of CPUs to consume.
    cpu: 1
    # -- The max amount of RAM to consume.
    memory: "200Mi"
  requests:
    # -- The amount of CPUs which has to be available on the scheduled node.
    cpu: "100Mi"
    # -- The amount of RAM which has to be available on the scheduled node.
    memory: "100Mi"

# Define and create Kubernetes Service.
#
# Ref.: https://kubernetes.io/docs/concepts/services-networking/service
service:
  # -- Additional custom annotations
  annotations: {}

  # -- Enable kubernetes service creation.
  enabled: true

  # "ClusterIP" => Exposes the Service on a cluster-internal IP. Choosing this value makes the Service only reachable
  #                from within the cluster. This is the default that is used if you don't explicitly specify a type for
  #                a Service.
  # "NodePort" => Exposes the Service on each Node's IP at a static port (the NodePort). To make the node port
  #               available, Kubernetes sets up a cluster IP address, the same as if you had requested a Service of
  #               type: ClusterIP.
  # "LoadBalancer" => Exposes the Service externally using a cloud provider's load balancer.
  #
  # Ref.: https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types

  # -- Choose the kind of Service, one of "ClusterIP", "NodePort" or "LoadBalancer".
  type: "ClusterIP"

  # Define the ports of Service.
  # You can set the port value to an arbitrary value, it will map the container port by name.
  #
  ports:
    well-known:
      # -- Accessible port.
      port: 8080
      # -- Internal port.
      containerPort: 8080
      # -- Service protocol.
      protocol: "TCP"

# Service account to use.
# Ref.: https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/
serviceAccount:
  # -- Additional custom annotations for the ServiceAccount.
  annotations: {}

  # -- Allows auto mount of ServiceAccountToken on the serviceAccount created. Can be set to false if pods using this
  # serviceAccount do not need to use K8s API.
  automountServiceAccountToken: false

  # -- Enable creation of ServiceAccount for pod.
  create: true

  # -- Additional custom labels for the ServiceAccount.
  labels: {}

# -- In seconds, time the given to the pod needs to terminate gracefully.
# Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods
terminationGracePeriodSeconds: 30

# -- Tolerations for pod assignment
# Ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
tolerations: []

# -- Topology spread constraints rely on node labels to identify the topology domain(s) that each Node is in
# Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-topology-spread-constraints/
#
# topologySpreadConstraints:
#   - maxSkew: 1
#     topologyKey: failure-domain.beta.kubernetes.io/zone
#     whenUnsatisfiable: DoNotSchedule
topologySpreadConstraints: []

# Set up update strategy.
#
# Ref: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#strategy
#
# Example:
# updateStrategy:
#  type: RollingUpdate
#  rollingUpdate:
#    maxSurge: 25%
#    maxUnavailable: 25%
updateStrategy:
  # RollingUpdate strategy configuration
  rollingUpdate:
    # -- Specifies the maximum number of Pods that can be created over the desired number of Pods.
    maxSurge: 2

    # -- Specifies the maximum number of Pods that can be unavailable during the update process
    maxUnavailable: 1

  # -- Set to Recreate if you use persistent volume that cannot be mounted by more than one pods to make sure the pods
  # is destroyed first.
  type: "RollingUpdate"
...
