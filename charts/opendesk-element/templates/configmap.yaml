{{/*
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
*/}}
---
kind: "ConfigMap"
apiVersion: "v1"
metadata:
  name: {{ include "common.names.fullname" . | quote }}
  labels:
    {{- include "common.labels.standard" . | nindent 4 }}
    {{- if .Values.commonLabels }}
    {{- include "common.tplvalues.render" ( dict "value" .Values.commonLabels "context" $ ) | nindent 4 }}
    {{- end }}
  {{- if .Values.commonAnnotations }}
  annotations: {{- include "common.tplvalues.render" ( dict "value" .Values.commonAnnotations "context" $ ) | nindent 4 }}
  {{- end }}
data:
  config.json: |-
    {
        "default_server_config": {
            "m.homeserver": {
                "base_url": {{ .Values.configuration.homeserver.baseUrl | default (printf "https://%s.%s" .Values.global.hosts.synapse .Values.global.domain) | quote }}
            }
        },
        "sso_redirect_options": {
            "on_welcome_page": true
        },
        "jitsi": {
            "preferred_domain": "{{ .Values.global.hosts.jitsi }}.{{ .Values.global.domain }}"
        },
        "jitsi_widget": {
            "skip_built_in_welcome_screen": true
        },
        {{- if .Values.ingress.tls.enabled }}
        "permalink_prefix": "https://{{ coalesce .Values.ingress.host (printf "%s.%s" .Values.global.hosts.element .Values.global.domain) }}",
        {{- else }}
        "permalink_prefix": "http://{{ coalesce .Values.ingress.host (printf "%s.%s" .Values.global.hosts.element .Values.global.domain) }}",
        {{- end }}
        "default_federate": false,
        "disable_custom_urls": true,
        "integrations_ui_url": "",
        "integrations_rest_url": "",
        "integrations_widgets_urls": [],
        "features": {
            "feature_rust_crypto": {{ .Values.configuration.endToEndEncryption }},
            "feature_video_rooms": false,
            "feature_ask_to_join": true
        },
        "enable_presence_by_hs_url": {
            "https://matrix.org": false,
            "https://matrix-client.matrix.org": false
        },
        "setting_defaults": {
            "UIFeature.feedback": false,
            "UIFeature.advancedSettings": false,
            "UIFeature.shareQrCode": false,
            "UIFeature.shareSocial": false,
            "UIFeature.identityServer": false,
            "UIFeature.thirdPartyId": false,
            "UIFeature.registration": false,
            "UIFeature.passwordReset": false,
            "UIFeature.deactivate": false,
            "UIFeature.advancedEncryption": false,
            "UIFeature.roomHistorySettings": false,
            "MessageComposerInput.showStickersButton": false,
            "breadcrumbs": false,
            "fallbackICEServerAllowed": false
        },
        "default_theme": "light",
        {{- if .Values.theme.title }}
        "brand": {{ .Values.theme.title | quote }},
        {{- end }}
        {{- if .Values.theme.imagery.logoHeaderSvg }}
        "branding": {
            "auth_header_logo_url": "data:image/svg+xml;base64,{{ .Values.theme.imagery.logoHeaderSvg | b64enc }}",
            "welcome_background_url": "data:image/gif;base64,R0lGODlhAQABAPAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
        },
        {{- end }}
        "custom_translations_url": "/custom-translations.json",
        {{- if .Values.configuration.welcomeUserId }}
        "embeddedPages": {
          "homeUrl": "/custom-home.html"
        },
        {{- end }}
        "net.nordeck.element_web.module.widget_toggles": {
          "config": {
            "types": [
              "jitsi",
              "net.nordeck"
            ]
          }
        },
        "net.nordeck.element_web.module.guest": {
          "config": {
            "guest_user_homeserver_url": {{ .Values.configuration.homeserver.baseUrl | default (printf "https://%s.%s" .Values.global.hosts.synapse .Values.global.domain) | quote }}
          }
        },
        {{- with .Values.configuration.additionalConfiguration }}
        {{- toJson . | trimPrefix "{" | trimSuffix "}" | nindent 8 }},
        {{- end }}
        "dummy_end": "placeholder"
    }
  custom-home.html: |-
    <style type="text/css">
      .mx_HomePage_body {
          display: flex;
          flex-direction: column;
          align-items: center;
          justify-content: center;
          height: 100%;
      }
    </style>

    {{- if .Values.theme.imagery.logoPortalBackgroundSvg }}
    <img width="300px" src="data:image/svg+xml;base64,{{ .Values.theme.imagery.logoPortalBackgroundSvg | b64enc }}">
    {{- end }}
    <h1>Willkommen</h1>
    <a class="mx_AccessibleButton mx_AccessibleButton_hasKind mx_AccessibleButton_kind_primary" href="./#/user/{{ .Values.configuration.welcomeUserId }}?action=chat">Terminplaner öffnen</a>
  custom-translations.json: |-
    {
      "power_level|default": {
        "de": "Teilnehmer*in",
        "en": "Participant"
      },
      "power_level|moderator": {
        "de": "Assistent*in",
        "en": "Assistant"
      },
      "power_level|mod": {
        "de": "Assistent*in",
        "en": "Assistant"
      },
      "power_level|admin": {
        "de": "Moderator*in",
        "en": "Moderator"
      },
      "room|header|n_people_asking_to_join": {
        "de": {
          "one": "Möchte beitreten",
          "other": "%(count)s Personen möchten beitreten"
        },
        "en": {
          "one": "Asking to join",
          "other": "%(count)s people asking to join"
        }
      },
      "room|knock_denied_subtitle": {
        "de": "Da deine Beitrittsanfrage abgelehnt wurde, bitten wir um Geduld, bis du eine Einladung vom Administrator oder Moderator erhältst.",
        "en": "As you have been denied access, you cannot rejoin unless you are invited by the admin or moderator of the group."
      },
      "room|knock_denied_title": {
        "de": "Deine Beitrittsanfrage wurde abgelehnt",
        "en": "You have been denied access"
      },
      "room_settings|security|publish_room": {
        "de": "Diesen Raum im Raumverzeichnis veröffentlichen.",
        "en": "Make this room visible in the public room directory."
      }
    }
---
kind: "ConfigMap"
apiVersion: "v1"
metadata:
  name: "{{ include "common.names.fullname" . }}-nginx"
  labels:
    {{- include "common.labels.standard" . | nindent 4 }}
    {{- if .Values.commonLabels }}
    {{- include "common.tplvalues.render" ( dict "value" .Values.commonLabels "context" $ ) | nindent 4 }}
    {{- end }}
  {{- if .Values.commonAnnotations }}
  annotations: {{- include "common.tplvalues.render" ( dict "value" .Values.commonAnnotations "context" $ ) | nindent 4 }}
  {{- end }}
data:
  nginx.conf: |-
    worker_processes  auto;

    error_log  stderr warn;
    pid        /tmp/nginx.pid;

    events {
        worker_connections  1024;
    }

    http {
        client_body_temp_path /tmp/client_temp;
        proxy_temp_path       /tmp/proxy_temp_path;
        fastcgi_temp_path     /tmp/fastcgi_temp;
        uwsgi_temp_path       /tmp/uwsgi_temp;
        scgi_temp_path        /tmp/scgi_temp;

        include       /etc/nginx/mime.types;
        default_type  application/octet-stream;

        set_real_ip_from 0.0.0.0/0;
        real_ip_header X-Forwarded-For;

        log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                          '$status $body_bytes_sent "$http_referer" '
                          '"$http_user_agent"';

        access_log  /var/log/nginx/access.log  main;

        sendfile        on;
        #tcp_nopush     on;

        keepalive_timeout  65;

        server {
            listen       8080;

            add_header X-Frame-Options SAMEORIGIN;
            add_header X-Content-Type-Options nosniff;
            add_header X-XSS-Protection "1; mode=block";
            add_header Content-Security-Policy "frame-ancestors 'self'";
            add_header X-Robots-Tag "noindex, nofollow, noarchive, noimageindex";

            location / {
                root   /usr/share/nginx/html;
                index  index.html index.htm;

                charset utf-8;
            }

            location = /health {
                allow all;
                default_type 'application/json';
                return 200 '{"status": "ok"}';
            }

        }
    }
...
