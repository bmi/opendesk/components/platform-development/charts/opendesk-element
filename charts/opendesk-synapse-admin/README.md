<!--
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-License-Identifier: Apache-2.0
-->
# opendesk-synapse-admin

Deploy Matrix/Synapse Admin.

## Installing the Chart

To install the chart with the release name `my-release`, you have two options:

### Install via Repository
```console
helm repo add opendesk-element https://gitlab.opencode.de/api/v4/projects/2291/packages/helm/stable
helm install my-release --version 6.1.0 opendesk-element/opendesk-synapse-admin
```

### Install via OCI Registry
```console
helm repo add opendesk-element oci://registry.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element
helm install my-release --version 6.1.0 opendesk-element/opendesk-synapse-admin
```

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| oci://registry.opencode.de/bmi/opendesk/components/external/charts/bitnami-charts | common | ^2.x.x |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Affinity for pod assignment Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity Note: podAffinityPreset, podAntiAffinityPreset, and  nodeAffinityPreset will be ignored when it's set |
| cleanup.deletePodsOnSuccess | bool | `true` | Keep Pods/Job logs after successful run. |
| cleanup.deletePodsOnSuccessTimeout | int | `180` | When deletePodsOnSuccess is enabled, the pod will be deleted after configured seconds. |
| commonAnnotations | object | `{}` | Additional custom annotations to add to all deployed objects. |
| commonLabels | object | `{}` | Additional custom labels to add to all deployed objects. |
| configuration.adminBot.backupPhrase | string | `""` | Backup Passphrase for adminbot |
| configuration.adminBot.name | string | `"adminbot"` | Name of adminbot user |
| configuration.adminBot.secretKey | string | `"access_token"` | Key of secret where adminbot access token is stored |
| configuration.adminBot.secretName | string | `"matrix-adminbot-account"` | Secret name where adminbot access token is stored |
| configuration.auditBot.backupPhrase | string | `""` |  |
| configuration.auditBot.name | string | `"auditbot"` | Name of auditbot user |
| configuration.database.channelBinding | string | `"prefer"` | This option controls the client's use of channel binding. |
| configuration.database.clientEncoding | string | `"auto"` | This sets the client_encoding configuration parameter for this connection. |
| configuration.database.connectTimeout | int | `10` | Maximum time to wait while connecting, in seconds. |
| configuration.database.connectionPoolMax | string | `"5"` | The minimum number of connections in pool. |
| configuration.database.connectionPoolMin | string | `"3"` | The minimum number of connections in pool. |
| configuration.database.gssencmode | string | `"prefer"` | This option determines whether or with what priority a secure GSS TCP/IP connection will be negotiated with the server. |
| configuration.database.host | string | `"postgresql"` | External database host address. |
| configuration.database.keepalives | int | `1` | Controls whether client-side TCP keepalives are used. |
| configuration.database.keepalivesCount | int | `3` | Controls the number of TCP keepalives that can be lost before the client's connection to the server is considered dead |
| configuration.database.keepalivesIdle | int | `10` | Controls the number of seconds of inactivity after which TCP should send a keepalive message to the server. |
| configuration.database.keepalivesInterval | int | `10` | Controls the number of seconds after which a TCP keepalive message that is not acknowledged by the server should be retransmitted. |
| configuration.database.name | string | `"matrix"` | External database name. |
| configuration.database.password.existingSecret.key | string | `""` | Key in secret containing the password. |
| configuration.database.password.existingSecret.name | string | `""` | Secret name containing password (higher precedence than plain value). |
| configuration.database.password.value | string | `""` | External database password key as plain value. |
| configuration.database.port | int | `5432` | External database port. |
| configuration.database.replication | bool | `false` | This option determines whether the connection should use the replication protocol instead of the normal protocol. |
| configuration.database.requireAuth | list | `[]` | Specifies the authentication method that the client requires from the server. |
| configuration.database.sslMinProtocolVersion | string | `"TLSv1.2"` | This parameter specifies the minimum SSL/TLS protocol version to allow for the connection |
| configuration.database.sslcompression | int | `0` | If set to 1, data sent over SSL connections will be compressed. |
| configuration.database.sslmode | string | `"prefer"` | This option determines whether or with what priority a secure SSL TCP/IP connection will be negotiated with the server. |
| configuration.database.user | string | `"matrix_user"` | External database user. |
| configuration.homeserver.baseUrl | string | `""` | URL of synapse deployment. As default the url of synapse will be used. |
| configuration.homeserver.serverName | string | `""` | Name of synapse server. As default the value of global.domain will be used. |
| configuration.ldap | object | `{"attributes":{"name":"description","uid":"cn"},"base":"dc=example,dc=localhost","bind_dn":"cn=readonly,dc=example,dc=localhost","bind_password":"","filter":"(memberOf=cn=managed-by-attribute-LivecollaborationAdmin,cn=groups,dc=swp-ldap,dc=internal)","uri":"ldaps://ldap.example.localhost:636"}` | LDAP config |
| configuration.ldap.attributes.name | string | `"description"` | Use LDAP description as name. |
| configuration.ldap.attributes.uid | string | `"cn"` | Use LDAP cn as UID. |
| configuration.ldap.base | string | `"dc=example,dc=localhost"` | LDAP base. |
| configuration.ldap.bind_dn | string | `"cn=readonly,dc=example,dc=localhost"` | LDAP DN. |
| configuration.ldap.bind_password | string | `""` | LDAP password. |
| configuration.ldap.filter | string | `"(memberOf=cn=managed-by-attribute-LivecollaborationAdmin,cn=groups,dc=swp-ldap,dc=internal)"` | LDAP filter to find all admins |
| configuration.ldap.uri | string | `"ldaps://ldap.example.localhost:636"` | LDAP URI. |
| containerSecurityContext.allowPrivilegeEscalation | bool | `false` | Enable container privileged escalation. |
| containerSecurityContext.capabilities | object | `{"drop":["ALL"]}` | Additional capabilities. |
| containerSecurityContext.enabled | bool | `true` | Enable security context. |
| containerSecurityContext.privileged | bool | `false` | Run container in privileged mode. |
| containerSecurityContext.readOnlyRootFilesystem | bool | `true` | Mounts the container's root filesystem as read-only. |
| containerSecurityContext.runAsNonRoot | bool | `true` | Run container as user. |
| containerSecurityContext.runAsUser | int | `10019` | Process user id. |
| containerSecurityContext.seccompProfile.type | string | `"RuntimeDefault"` | Disallow custom Seccomp profile by setting it to RuntimeDefault. |
| cron.activeDeadlineSeconds | int | `60` | Specify the max execution time of a cron pod. |
| cron.affinity | object | `{}` | Affinity for pod assignment. Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity Note: podAffinityPreset, podAntiAffinityPreset, and nodeAffinityPreset will be ignored when it's set. |
| cron.concurrencyPolicy | string | `"Forbid"` | It specifies how to treat concurrent executions of a job that is created by this CronJob. The spec may specify only one of the following concurrency policies: "Allow", "Forbid", "Replace". |
| cron.containerSecurityContext.allowPrivilegeEscalation | bool | `false` | Enable container privileged escalation. |
| cron.containerSecurityContext.capabilities | object | `{"drop":["ALL"]}` | Security capabilities for container. |
| cron.containerSecurityContext.enabled | bool | `true` | Enable security context. |
| cron.containerSecurityContext.readOnlyRootFilesystem | bool | `true` | Mounts the container's root filesystem as read-only. |
| cron.containerSecurityContext.runAsGroup | int | `123` | Process group id. |
| cron.containerSecurityContext.runAsNonRoot | bool | `true` | Run container as a user. |
| cron.containerSecurityContext.runAsUser | int | `123` | Process user id. |
| cron.containerSecurityContext.seccompProfile.type | string | `"RuntimeDefault"` | Disallow custom Seccomp profile by setting it to RuntimeDefault. |
| cron.enabled | bool | `true` | Enable cron job. |
| cron.extraEnvVars | list | `[]` | Array with extra environment variables to add to cron container.  extraEnvVars:   - name: FOO     value: "bar"  |
| cron.extraVolumeMounts | list | `[]` | Optionally specify extra list of additional volumeMounts. |
| cron.extraVolumes | list | `[]` | Optionally specify extra list of additional volumes |
| cron.failedJobsHistoryLimit | int | `1` | These fields specify how many failed jobs should be kept. Setting a limit to 0 corresponds to keeping none of the jobs after they finish. |
| cron.image.imagePullPolicy | string | `"IfNotPresent"` | Define an ImagePullPolicy.  Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy  "IfNotPresent" => The image is pulled only if it is not already present locally. "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to             resolve the name to an image digest. If the kubelet has a container image with that exact digest cached             locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the resolved             digest, and uses that image to launch the container. "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the            kubelet attempts to start the container; otherwise, startup fails  |
| cron.image.registry | string | `""` | Container registry address. This setting has higher precedence than global.registry. |
| cron.image.repository | string | `"bmi/opendesk/components/platform-development/images/opendesk-element-syncadmins"` | Container repository string. |
| cron.image.tag | string | `"1.0.3@sha256:1dea24d5f65a6f9ac63b402c772dd81dcd07a847d24845901c8a039461043097"` | Define image tag. |
| cron.imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| cron.lifecycleHooks | object | `{}` | Lifecycle to automate configuration before or after startup |
| cron.nodeSelector | object | `{}` | Node labels for pod assignment. Ref: https://kubernetes.io/docs/user-guide/node-selection/ |
| cron.podSecurityContext.enabled | bool | `true` | Enable security context. |
| cron.podSecurityContext.fsGroup | int | `65532` | If specified, all processes of the container are also part of the supplementary group. |
| cron.podSecurityContext.fsGroupChangePolicy | string | `"Always"` | Change ownership and permission of the volume before being exposed inside a Pod. |
| cron.resources.limits.cpu | int | `1` | The max amount of CPUs to consume. |
| cron.resources.limits.memory | string | `"50Mi"` | The max amount of RAM to consume. |
| cron.resources.requests.cpu | string | `"50m"` | The amount of CPUs which has to be available on the scheduled node. |
| cron.resources.requests.memory | string | `"50Mi"` | The amount of RAM which has to be available on the scheduled node. |
| cron.restartPolicy | string | `"Never"` | Container restart policy. |
| cron.schedule | string | `"*/5 * * * *"` | Cron schedule. |
| cron.successfulJobsHistoryLimit | int | `0` | These fields specify how many successful jobs should be kept. Setting a limit to 0 corresponds to keeping none of the jobs after they finish. |
| cron.terminationGracePeriodSeconds | int | `30` | In seconds, time the given to the pod needs to terminate gracefully. Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods |
| cron.tolerations | list | `[]` | Tolerations for pod assignment. Ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/ |
| cron.topologySpreadConstraints | list | `[]` | Topology spread constraints rely on node labels to identify the topology domain(s) that each Node is in. Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-topology-spread-constraints/  topologySpreadConstraints:   - maxSkew: 1     topologyKey: failure-domain.beta.kubernetes.io/zone     whenUnsatisfiable: DoNotSchedule |
| extraEnvVars | list | `[]` | Array with extra environment variables to add to containers.  extraEnvVars:   - name: FOO     value: "bar"  |
| extraVolumeMounts | list | `[]` | Optionally specify extra list of additional volumeMounts. |
| extraVolumes | list | `[]` | Optionally specify extra list of additional volumes |
| fullnameOverride | string | `""` | Provide a name to substitute for the full names of resources. |
| global.domain | string | `"example.com"` | The Top-Level-Domain (TLD) name which is used in f.e. in Ingress component. |
| global.hosts.adminBot | string | `"adminbot"` | Subdomain for adminbot, results in "https://{{ adminbot }}.{{ domain}}" |
| global.hosts.element | string | `"element"` | Subdomain for element, results in "https://{{ element }}.{{ domain }}". |
| global.hosts.synapse | string | `"synapse"` | Subdomain for synapse, results in "https://{{ synapse }}.{{ domain }}". |
| global.hosts.synapseAdmin | string | `"synapse-admin"` | Subdomain for synapse admin, results in "https://{{ synapse-admin }}.{{ domain}}" |
| global.imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| global.registry | string | `"registry.opencode.de"` | Container registry address. |
| image.imagePullPolicy | string | `"IfNotPresent"` | Define an ImagePullPolicy.  Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy  "IfNotPresent" => The image is pulled only if it is not already present locally. "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to             resolve the name to an image digest. If the kubelet has a container image with that exact digest cached             locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the resolved             digest, and uses that image to launch the container. "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the            kubelet attempts to start the container; otherwise, startup fails  |
| image.registry | string | `""` | Container registry address. This setting has higher precedence than global.registry. |
| image.repository | string | `"zendis/opendesk-enterprise/components/supplier/element/images-mirror/synapse_admin"` | Container repository string. |
| image.tag | string | `"v16.105.4@sha256:ad5fc0484715ff3ee0d00837b66241647ced77af05396661ca552847d12babb6"` | Define image tag. |
| imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| ingress.annotations | object | `{}` | Define custom ingress annotations.  annotations:   nginx.ingress.kubernetes.io/rewrite-target: /  |
| ingress.enabled | bool | `true` | Enable create of Ingress. |
| ingress.host | string | `""` | Define the Fully Qualified Domain Name (FQDN) where adminbot should be reachable. This setting has higher precedence than global.registry. |
| ingress.ingressClassName | string | `"nginx"` | The Ingress controller class name. |
| ingress.path | string | `"/"` | Define the Ingress path. |
| ingress.pathType | string | `"Prefix"` | Each path in an Ingress is required to have a corresponding path type. Paths that do not include an explicit pathType will fail validation. There are three supported path types:  "ImplementationSpecific" => With this path type, matching is up to the IngressClass. Implementations can treat this                             as a separate pathType or treat it identically to Prefix or Exact path types. "Exact" => Matches the URL path exactly and with case sensitivity. "Prefix" => Matches based on a URL path prefix split by /.  Ref.: https://kubernetes.io/docs/concepts/services-networking/ingress/#path-types  |
| ingress.tls | object | `{"enabled":true,"secretName":""}` | Secure an Ingress by specifying a Secret that contains a TLS private key and certificate.  Ref.: https://kubernetes.io/docs/concepts/services-networking/ingress/#tls  |
| ingress.tls.enabled | bool | `true` | Enable TLS/SSL/HTTPS for Ingress.  |
| ingress.tls.secretName | string | `""` | The name of the kubernetes secret which contains a TLS private key and certificate. Hint: This secret is not created by this chart and must be provided. |
| lifecycleHooks | object | `{}` | Lifecycle to automate configuration before or after startup |
| livenessProbe.enabled | bool | `true` | Enables kubernetes LivenessProbe. |
| livenessProbe.failureThreshold | int | `3` | Number of failed executions until container is terminated. |
| livenessProbe.initialDelaySeconds | int | `1` | Delay after container start until LivenessProbe is executed. |
| livenessProbe.periodSeconds | int | `10` | Time between probe executions. |
| livenessProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| livenessProbe.timeoutSeconds | int | `1` | Timeout for command return. |
| nameOverride | string | `""` | String to partially override release name. |
| nodeSelector | object | `{}` | Node labels for pod assignment Ref: https://kubernetes.io/docs/user-guide/node-selection/ |
| podAnnotations | object | `{}` | Pod Annotations. Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/ |
| podLabels | object | `{}` | Pod Labels. Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/ |
| podSecurityContext.enabled | bool | `true` | Enable security context. |
| podSecurityContext.fsGroup | int | `10019` | If specified, all processes of the container are also part of the supplementary group |
| readinessProbe.enabled | bool | `true` | Enables kubernetes ReadinessProbe. |
| readinessProbe.failureThreshold | int | `3` | Number of failed executions until container is terminated. |
| readinessProbe.initialDelaySeconds | int | `1` | Delay after container start until ReadinessProbe is executed. |
| readinessProbe.periodSeconds | int | `10` | Time between probe executions. |
| readinessProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| readinessProbe.timeoutSeconds | int | `1` | Timeout for command return. |
| replicaCount | int | `1` | Set the amount of replicas of statefulset. |
| resources.limits.cpu | int | `4` | The max amount of CPUs to consume. |
| resources.limits.memory | string | `"500Mi"` | The max amount of RAM to consume. |
| resources.requests.cpu | string | `"50m"` | The amount of CPUs which has to be available on the scheduled node. |
| resources.requests.memory | string | `"50Mi"` | The amount of RAM which has to be available on the scheduled node. |
| service.annotations | object | `{}` | Additional custom annotations |
| service.clusterIP | string | `"None"` | Set the clusterIP to None to create a Headless Service. |
| service.enabled | bool | `true` | Enable kubernetes service creation. |
| service.ports.web.containerPort | int | `3000` | Internal port. |
| service.ports.web.port | int | `80` | Accessible port. |
| service.ports.web.protocol | string | `"TCP"` | Service protocol. |
| service.type | string | `"ClusterIP"` | Choose the kind of Service, one of "ClusterIP", "NodePort" or "LoadBalancer". |
| serviceAccount.annotations | object | `{}` | Additional custom annotations for the ServiceAccount. |
| serviceAccount.automountServiceAccountToken | bool | `false` | Allows auto mount of ServiceAccountToken on the serviceAccount created. Can be set to false if pods using this serviceAccount do not need to use K8s API. |
| serviceAccount.create | bool | `true` | Enable creation of ServiceAccount for pod. |
| serviceAccount.labels | object | `{}` | Additional custom labels for the ServiceAccount. |
| startupProbe.enabled | bool | `true` | Enables kubernetes StartupProbe. |
| startupProbe.failureThreshold | int | `3` | Number of failed executions until container is terminated. |
| startupProbe.initialDelaySeconds | int | `5` | Delay after container start until StartupProbe is executed. |
| startupProbe.periodSeconds | int | `10` | Time between probe executions. |
| startupProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| startupProbe.timeoutSeconds | int | `1` | Timeout for command return. |
| terminationGracePeriodSeconds | int | `30` | In seconds, time the given to the pod needs to terminate gracefully. Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods |
| tolerations | list | `[]` | Tolerations for pod assignment Ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/ |
| topologySpreadConstraints | list | `[]` | Topology spread constraints rely on node labels to identify the topology domain(s) that each Node is in Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-topology-spread-constraints/  topologySpreadConstraints:   - maxSkew: 1     topologyKey: failure-domain.beta.kubernetes.io/zone     whenUnsatisfiable: DoNotSchedule |
| updateStrategy.type | string | `"RollingUpdate"` | Set to Recreate if you use persistent volume that cannot be mounted by more than one pods to make sure the pods is destroyed first. |

## Uninstalling the Chart

To install the release with name `my-release`:

```bash
helm uninstall my-release
```

## Signing

Helm charts are signed with helm native signing method.

You can verify the chart against [the public GPG key](../../files/gpg-pubkeys/opendesk.gpg).

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright (C) 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
