# SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0
---
# The global properties are used to configure multiple charts at once.
global:
  # -- The Top-Level-Domain (TLD) name which is used in f.e. in Ingress component.
  domain: "example.com"

  # Define the Subdomain for components used in f.e. in Ingress component.
  hosts:
    # -- Subdomain for element, results in "https://{{ element }}.{{ domain }}".
    element: "element"
    # -- Subdomain for synapse, results in "https://{{ synapse }}.{{ domain }}".
    synapse: "synapse"

  # -- Container registry address.
  registry: "docker.io"

  # -- Credentials to fetch images from private registry
  # Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
  #
  # imagePullSecrets:
  #   - "docker-registry"
  #
  imagePullSecrets: []

# -- Affinity for pod assignment
# Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity
# Note: podAffinityPreset, podAntiAffinityPreset, and  nodeAffinityPreset will be ignored when it's set
affinity: {}

# -- Additional custom annotations to add to all deployed objects.
commonAnnotations: {}

# -- Additional custom labels to add to all deployed objects.
commonLabels: {}

# Synapse specific configuration
configuration:
  # -- Additional configuration to place into homeserver.yaml
  additionalConfiguration: {}

  # Database connection settings.
  database:
    # -- External database host address.
    host: "postgresql"

    # -- External database name.
    name: "matrix"

    # -- External database user.
    user: "matrix_user"

    # External database password.
    password:
      # -- External database password key as plain value.
      value: ""

      # Credential from existing secret (higher precedence than plain value).
      existingSecret:
        # -- Secret name containing password (higher precedence than plain value).
        name: ""

        # -- Key in secret containing the password.
        key: ""

    # -- External database port.
    port: 5432

    # -- Specifies the authentication method that the client requires from the server.
    requireAuth: []

    # -- This option controls the client's use of channel binding.
    channelBinding: "prefer"

    # -- Maximum time to wait while connecting, in seconds.
    connectTimeout: 10

    # -- This sets the client_encoding configuration parameter for this connection.
    clientEncoding: "auto"

    # -- Controls whether client-side TCP keepalives are used.
    keepalives: 1

    # -- Controls the number of seconds of inactivity after which TCP should send a keepalive message to the server.
    keepalivesIdle: 10

    # -- Controls the number of seconds after which a TCP keepalive message that is not acknowledged by the server
    # should be retransmitted.
    keepalivesInterval: 10

    # -- Controls the number of TCP keepalives that can be lost before the client's connection to the server is
    # considered dead
    keepalivesCount: 3

    # -- This option determines whether the connection should use the replication protocol instead of the normal
    # protocol.
    replication: false

    # -- This option determines whether or with what priority a secure GSS TCP/IP connection will be negotiated with the
    # server.
    gssencmode: "prefer"

    # -- This option determines whether or with what priority a secure SSL TCP/IP connection will be negotiated with the
    # server.
    sslmode: "prefer"

    # -- If set to 1, data sent over SSL connections will be compressed.
    sslcompression: 0

    # -- This parameter specifies the minimum SSL/TLS protocol version to allow for the connection
    sslMinProtocolVersion: "TLSv1.2"

    # -- The minimum number of connections in pool.
    connectionPoolMin: "3"

    # -- The minimum number of connections in pool.
    connectionPoolMax: "5"

  # Settings regarding homeserver.
  homeserver:
    # -- A list of app service registration files that should be enabled in the homeserver
    # Ref.: https://matrix-org.github.io/synapse/latest/application_services.html
    appServiceConfigs: []

    # Enable to allow information about the user presence status to be shared.
    # Ref.: https://element-hq.github.io/synapse/latest/usage/configuration/config_documentation.html#presence
    presence:
      enabled: false
    # -- URL of synapse deployment. As default the url of synapse will be used.
    baseUrl: ""

    # Keycloak compatible OIDC settings.
    oidc:
      # -- OAUTH2 client id to use.
      clientId: "matrix"

      # -- OAUTH2 client secret to use.
      clientSecret: ""

      # -- Enable keycloak OIDC client.
      enabled: true

      # -- The OIDC issuer. Used to validate tokens and (if discovery is enabled) to discover the provider's endpoints.
      issuer: ""

      # -- List of scopes to request. This should normally include the "openid" scope.
      scopes:
        - "openid"
        - "profile"
        - "email"

      # -- Which OIDC `user` claim to use as localpart for the MatrixIDs. The localpart is only set on account creation.
      # Afterwards the user is identified by the `subject_template` which the LDAP entryUUID is used for. So changes
      # to the username will not affect the account, as the localpart of the MatrixID stays as it is (or was).
      matrixIdLocalpart: "opendesk_useruuid"

    # User profile related settings.
    profile:
      # -- Set to true if you want to allow users to update their own display name.
      allowUsersToUpdateDisplayname: false

    # -- Macaroon Secret Key.
    # Ref.: https://matrix-org.github.io/synapse/latest/usage/configuration/config_documentation.html
    macaroonSecretKey: ""

    # -- Registration Shared Secret.
    # Ref.: https://matrix-org.github.io/synapse/latest/usage/configuration/config_documentation.html
    registrationSharedSecret: ""

    # SMTP setting for notifications. STARTTLS is required.
    smtp:
      # -- Enable sending of mails for Synapse. If it is not enabled the user still has the option
      # to set notification preferences in the Element frontend, but notifications will not be sent.
      # Ensure you set the other required properties as well.
      enabled: true
      # -- The mail address notifications are sent from.
      senderAddress:
      # -- The SMTP relay host.
      host: ""
      # -- The SMTP relay host's port.
      port: 587
      # -- Require SSL/TLS encryption for connection.
      tls: false
      # -- Use StartTLS for traffic encryption.
      starttls: true
      # -- The username for SMTP authentication at the relay host.
      username: ""
      # -- The password for SMTP authentication at the relay host.
      password: ""

    # -- Name of synapse server. As default the name of synapse will be used.
    serverName: ""

    # -- Signing Key.
    # Ref.: https://matrix-org.github.io/synapse/latest/usage/configuration/config_documentation.html
    signingKey: ""

    # Synapse TURN configuration:
    # Ref.: https://matrix-org.github.io/synapse/latest/turn-howto.html#synapse-setup
    turn:
      # -- Whether guests should be allowed to use the TURN server. This defaults to true, otherwise VoIP will be
      # unreliable for guests
      allowGuests: true

      # -- The Username and password if the TURN server needs them and does not use a token.
      credentials:
        password: ""
        username: ""

      # -- Enable custom turn server.
      enabled: true

      # -- The shared secret used to compute passwords for the TURN server.
      sharedSecret: ""

      # -- The public URIs of the TURN server to give to clients
      servers:
        - server: "turn.matrix.org"
          transport: "udp"
          port: 3478
        - server: "turn.matrix.org"
          transport: "tcp"
          port: 3478

      # -- How long generated TURN credentials last.
      userLifetime: "1h"

    # Settings for the guest module.
    # Ref.: https://github.com/nordeck/element-web-modules/tree/main/packages/synapse-guest-module
    guestModule:
      # -- Enable the guest module.
      enabled: false

      # The container image with the guest module
      image:
        # -- Container registry address. This setting has higher precedence than global.registry.
        registry: "ghcr.io"

        # -- Container repository string.
        repository: "nordeck/synapse-guest-module"

        # -- Define an ImagePullPolicy.
        #
        # Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy
        #
        # "IfNotPresent" => The image is pulled only if it is not already present locally.
        # "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to
        #             resolve the name to an image digest. If the kubelet has a container image with that exact digest
        #             cached locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the
        #             resolved digest, and uses that image to launch the container.
        # "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the
        #            kubelet attempts to start the container; otherwise, startup fails
        #
        imagePullPolicy: "IfNotPresent"

        # -- Define image tag.
        tag: "latest"

      # -- Configurations for the guest module.
      # https://github.com/nordeck/element-web-modules/tree/main/packages/synapse-guest-module#module-configuration
      configuration:
        display_name_suffix: " (Gast)"

# Security Context.
# Ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
containerSecurityContext:
  # -- Enable container privileged escalation.
  allowPrivilegeEscalation: false

  # -- Additional capabilities.
  capabilities:
    drop:
      - "ALL"

  # -- Enable security context.
  enabled: true

  # -- Run container in privileged mode.
  privileged: false

  # -- Mounts the container's root filesystem as read-only.
  readOnlyRootFilesystem: true

  # -- Process group id.
  # runAsGroup: 101

  # -- Run container as user.
  runAsNonRoot: true

  # -- Process user id.
  runAsUser: 10991

  # Set Seccomp profile.
  seccompProfile:
    # -- Disallow custom Seccomp profile by setting it to RuntimeDefault.
    type: "RuntimeDefault"

# -- Array with extra environment variables to add to containers.
#
# extraEnvVars:
#   - name: FOO
#     value: "bar"
#
extraEnvVars: []

# -- Optionally specify extra list of additional volumes
extraVolumes: []

# -- Optionally specify extra list of additional volumeMounts.
extraVolumeMounts: []

# Matrix federation
federation:
  # -- Enable matrix federation service and ingress.
  enabled: true

  # -- Allow list for domains of Matrix homeservers to federate with.
  #
  # Ref.: https://element-hq.github.io/synapse/latest/usage/configuration/config_documentation.html#federation_domain_whitelist
  domainAllowList: []

  # Define and create Kubernetes Ingress.
  #
  # Ref.: https://kubernetes.io/docs/concepts/services-networking/ingress/
  ingress:
    # -- Enable creation of Ingress.
    enabled: true

    # -- Define the Fully Qualified Domain Name (FQDN) where UCS Portal should be reachable. This setting has higher
    # precedence than global.registry.
    host: ""

    # -- Each path in an Ingress is required to have a corresponding path type. Paths that do not include an explicit
    # pathType will fail validation. There are three supported path types:
    #
    # "ImplementationSpecific" => With this path type, matching is up to the IngressClass. Implementations can treat
    #                             this as a separate pathType or treat it identically to Prefix or Exact path types.
    # "Exact" => Matches the URL path exactly and with case sensitivity.
    # "Prefix" => Matches based on a URL path prefix split by /.
    #
    # Ref.: https://kubernetes.io/docs/concepts/services-networking/ingress/#path-types
    #
    pathType: "Prefix"


    # -- The Ingress controller class name.
    ingressClassName: "nginx"

    # -- Define custom ingress annotations.
    #
    # annotations:
    #   nginx.ingress.kubernetes.io/rewrite-target: /
    #
    annotations: {}

    # -- Secure an Ingress by specifying a Secret that contains a TLS private key and certificate.
    #
    # Ref.: https://kubernetes.io/docs/concepts/services-networking/ingress/#tls
    #
    tls:
      # -- Enable TLS/SSL/HTTPS for Ingress.
      #
      enabled: true

      # -- The name of the kubernetes secret which contains a TLS private key and certificate.
      # Hint: This secret is not created by this chart and must be provided.
      secretName: ""


  # Define and create Kubernetes Service.
  #
  # Ref.: https://kubernetes.io/docs/concepts/services-networking/service
  service:
    # -- Additional custom annotations
    annotations: {}

    # "ClusterIP" => Exposes the Service on a cluster-internal IP. Choosing this value makes the Service only reachable
    #                from within the cluster. This is the default that is used if you don't explicitly specify a type
    #                for a Service.
    # "NodePort" => Exposes the Service on each Node's IP at a static port (the NodePort). To make the node port
    #               available, Kubernetes sets up a cluster IP address, the same as if you had requested a Service of
    #               type: ClusterIP.
    # "LoadBalancer" => Exposes the Service externally using a cloud provider's load balancer.
    #
    # Ref.: https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types

    # -- Choose the kind of Service, one of "ClusterIP", "NodePort" or "LoadBalancer".
    type: "ClusterIP"

    # Define the ports of Service.
    # You can set the port value to an arbitrary value, it will map the container port by name.
    #
    ports:
      federation:
        # -- Accessible port.
        port: 8448
        # -- Internal port.
        containerPort: 8448
        # -- Service protocol.
        protocol: "TCP"


# -- Provide a name to substitute for the full names of resources.
fullnameOverride: ""

# Container image section.
image:
  # -- Container registry address. This setting has higher precedence than global.registry.
  registry: ""

  # -- Container repository string.
  repository: "matrixdotorg/synapse"

  # -- Define an ImagePullPolicy.
  #
  # Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy
  #
  # "IfNotPresent" => The image is pulled only if it is not already present locally.
  # "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to
  #             resolve the name to an image digest. If the kubelet has a container image with that exact digest cached
  #             locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the resolved
  #             digest, and uses that image to launch the container.
  # "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the
  #            kubelet attempts to start the container; otherwise, startup fails
  #
  imagePullPolicy: "IfNotPresent"

  # -- Define image tag.
  tag: "v1.116.0@sha256:2d5c371f32694401734a8a7dccffaaf89aadee645df4f23e48bab48223cba5ea"

# -- Credentials to fetch images from private registry
# Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
#
# imagePullSecrets:
#   - "docker-registry"
#
imagePullSecrets: []

# -- Lifecycle to automate configuration before or after startup
lifecycleHooks: {}

# -- String to partially override release name.
nameOverride: ""

# -- Node labels for pod assignment
# Ref: https://kubernetes.io/docs/user-guide/node-selection/
nodeSelector: {}

#  Configure extra options for containers probes.
livenessProbe:
  # -- Enables kubernetes LivenessProbe.
  enabled: true
  # -- Number of failed executions until container is terminated.
  failureThreshold: 8
  # -- Delay after container start until LivenessProbe is executed.
  initialDelaySeconds: 1
  # -- Time between probe executions.
  periodSeconds: 6
  # -- Number of successful executions after failed ones until container is marked healthy.
  successThreshold: 1
  # -- Timeout for command return.
  timeoutSeconds: 2

# Database persistence settings.
persistence:
  # -- The volume access modes, some of "ReadWriteOnce", "ReadOnlyMany", "ReadWriteMany", "ReadWriteOncePod".
  #
  # "ReadWriteOnce" => The volume can be mounted as read-write by a single node. ReadWriteOnce access mode still can
  #                    allow multiple pods to access the volume when the pods are running on the same node.
  # "ReadOnlyMany" => The volume can be mounted as read-only by many nodes.
  # "ReadWriteMany" => The volume can be mounted as read-write by many nodes.
  # "ReadWriteOncePod" => The volume can be mounted as read-write by a single Pod. Use ReadWriteOncePod access mode if
  #                       you want to ensure that only one pod across whole cluster can read that PVC or write to it.
  #
  accessModes:
    - "ReadWriteOnce"

  # -- Annotations for the PVC.
  annotations: {}

  # -- Custom PVC data source.
  dataSource: {}

  # -- Enable data persistence (true) or use temporary storage (false).
  enabled: true

  # -- Use an already existing claim.
  existingClaim: ""

  # -- Labels for the PVC.
  labels: {}

  # -- The volume size with unit.
  size: "1Gi"

  # -- The (storage) class of PV.
  storageClass: ""

  # -- Selector to match an existing Persistent Volume (this value is evaluated as a template)
  #
  # selector:
  #   matchLabels:
  #     app: my-app
  #
  selector: {}

  # Temporary directory.
  temporaryDirectory:
    # -- Size limit of emptyDir volume.
    size: "50Mi"

# -- Pod Annotations.
# Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/
podAnnotations: {}

# -- Pod Labels.
# Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/
podLabels: {}

# Pod Security Context.
# Ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
podSecurityContext:
  # -- Enable security context.
  enabled: true

  # -- If specified, all processes of the container are also part of the supplementary group
  fsGroup: 10991

#  Configure extra options for containers probes.
readinessProbe:
  # -- Enables kubernetes ReadinessProbe.
  enabled: true
  # -- Delay after container start until ReadinessProbe is executed.
  initialDelaySeconds: 1
  # -- Number of failed executions until container is terminated.
  failureThreshold: 8
  # -- Time between probe executions.
  periodSeconds: 2
  # -- Number of successful executions after failed ones until container is marked healthy.
  successThreshold: 1
  # -- Timeout for command return.
  timeoutSeconds: 2

# -- Set the amount of replicas of statefulset.
replicaCount: 1

# Configure resource requests and limits.
#
# http://kubernetes.io/docs/user-guide/compute-resources/
#
resources:
  limits:
    # -- The max amount of CPUs to consume.
    cpu: 4
    # -- The max amount of RAM to consume.
    memory: "4Gi"
  requests:
    # -- The amount of CPUs which has to be available on the scheduled node.
    cpu: 1
    # -- The amount of RAM which has to be available on the scheduled node.
    memory: "2Gi"

# Define and create Kubernetes Service.
#
# Ref.: https://kubernetes.io/docs/concepts/services-networking/service
service:
  # -- Additional custom annotations
  annotations: {}

  # -- Set the clusterIP to None to create a Headless Service.
  clusterIP: "None"

  # -- Enable kubernetes service creation.
  enabled: true

  # "ClusterIP" => Exposes the Service on a cluster-internal IP. Choosing this value makes the Service only reachable
  #                from within the cluster. This is the default that is used if you don't explicitly specify a type for
  #                a Service.
  # "NodePort" => Exposes the Service on each Node's IP at a static port (the NodePort). To make the node port
  #               available, Kubernetes sets up a cluster IP address, the same as if you had requested a Service of
  #               type: ClusterIP.
  # "LoadBalancer" => Exposes the Service externally using a cloud provider's load balancer.
  #
  # Ref.: https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types

  # -- Choose the kind of Service, one of "ClusterIP", "NodePort" or "LoadBalancer".
  type: "ClusterIP"

  # Define the ports of Service.
  # You can set the port value to an arbitrary value, it will map the container port by name.
  #
  ports:
    synapse-http:
      # -- Accessible port.
      port: 8008
      # -- Internal port.
      containerPort: 8008
      # -- Service protocol.
      protocol: "TCP"
    synapse-health:
      # -- Accessible port.
      port: 8080
      # -- Internal port.
      containerPort: 8080
      # -- Service protocol.
      protocol: "TCP"
    synapse-repl:
      # -- Accessible port.
      port: 9093
      # -- Internal port.
      containerPort: 9093
      # -- Service protocol.
      protocol: "TCP"
    synapse-metrics:
      # -- Accessible port.
      port: 9001
      # -- Internal port.
      containerPort: 9001
      # -- Service protocol.
      protocol: "TCP"

# Service account to use.
# Ref.: https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/
serviceAccount:
  # -- Additional custom annotations for the ServiceAccount.
  annotations: {}

  # -- Allows auto mount of ServiceAccountToken on the serviceAccount created. Can be set to false if pods using this
  # serviceAccount do not need to use K8s API.
  automountServiceAccountToken: false

  # -- Enable creation of ServiceAccount for pod.
  create: true

  # -- Additional custom labels for the ServiceAccount.
  labels: {}

#  Configure extra options for containers probes.
startupProbe:
  # -- Enables kubernetes StartupProbe.
  enabled: true
  # -- Delay after container start until StartupProbe is executed.
  initialDelaySeconds: 5
  # -- Number of failed executions until container is terminated.
  failureThreshold: 10
  # -- Time between probe executions.
  periodSeconds: 2
  # -- Number of successful executions after failed ones until container is marked healthy.
  successThreshold: 1
  # -- Timeout for command return.
  timeoutSeconds: 1

# -- In seconds, time the given to the pod needs to terminate gracefully.
# Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods
terminationGracePeriodSeconds: 30

# -- Tolerations for pod assignment
# Ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
tolerations: []

# -- Topology spread constraints rely on node labels to identify the topology domain(s) that each Node is in
# Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-topology-spread-constraints/
#
# topologySpreadConstraints:
#   - maxSkew: 1
#     topologyKey: failure-domain.beta.kubernetes.io/zone
#     whenUnsatisfiable: DoNotSchedule
topologySpreadConstraints: []

# Set up update strategy.
#
# Ref: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#strategy
#
# Example:
# updateStrategy:
#  type: RollingUpdate
#  rollingUpdate:
#    maxSurge: 25%
#    maxUnavailable: 25%
updateStrategy:
  # -- Set to Recreate if you use persistent volume that cannot be mounted by more than one pods to make sure the pods
  # is destroyed first.
  type: "RollingUpdate"
...
