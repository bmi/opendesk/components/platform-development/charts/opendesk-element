{{/*
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
*/}}
{{- if .Values.service.enabled }}
---
apiVersion: "v1"
kind: "Service"
metadata:
  name: {{ include "common.names.fullname" . | quote }}
  namespace: {{ include "common.names.namespace" . | quote }}
  labels:
    {{- include "common.labels.standard" . | nindent 4 }}
    {{- if .Values.commonLabels }}
    {{- include "common.tplvalues.render" ( dict "value" .Values.commonLabels "context" $ ) | nindent 4 }}
    {{- end }}
  {{- if or .Values.service.annotations .Values.commonAnnotations }}
  annotations:
    {{- if .Values.service.annotations }}
    {{- include "common.tplvalues.render" ( dict "value" .Values.service.annotations "context" $ ) | nindent 4 }}
    {{- end }}
    {{- if .Values.commonAnnotations }}
    {{- include "common.tplvalues.render" ( dict "value" .Values.commonAnnotations "context" $ ) | nindent 4 }}
    {{- end }}
  {{- end }}
spec:
  type: {{ .Values.service.type | quote }}
  {{- with .Values.service.clusterIP }}
  clusterIP: {{ . }}
  {{- end }}
  ports:
    {{- range $key, $value := .Values.service.ports }}
    - name: {{ $key | quote }}
      port: {{ $value.port }}
      targetPort: {{ $value.port }}
      protocol: {{ $value.protocol | default "TCP" | quote }}
      {{- if and $value.nodePort (not (eq $.Values.service.type "ClusterIP")) }}
      nodePort: {{ $value.nodePort }}
      {{- end }}
    {{- end }}
  selector:
    {{- include "common.labels.matchLabels" . | nindent 4 }}
...
{{- end }}
{{- if .Values.federation.enabled }}
---
apiVersion: "v1"
kind: "Service"
metadata:
  name: "{{ include "common.names.fullname" . }}-federation"
  namespace: {{ include "common.names.namespace" . | quote }}
  labels:
    {{- include "common.labels.standard" . | nindent 4 }}
    {{- if .Values.commonLabels }}
    {{- include "common.tplvalues.render" ( dict "value" .Values.commonLabels "context" $ ) | nindent 4 }}
    {{- end }}
  {{- if or .Values.federation.service.annotations .Values.commonAnnotations }}
  annotations:
    {{- if .Values.federation.service.annotations }}
    {{- include "common.tplvalues.render" ( dict "value" .Values.federation.service.annotations "context" $ ) | nindent 4 }}
    {{- end }}
    {{- if .Values.commonAnnotations }}
    {{- include "common.tplvalues.render" ( dict "value" .Values.commonAnnotations "context" $ ) | nindent 4 }}
    {{- end }}
  {{- end }}
spec:
  type: {{ .Values.federation.service.type | quote }}
  ports:
    {{- range $key, $value := .Values.federation.service.ports }}
    - name: {{ $key | quote }}
      port: {{ $value.port }}
      targetPort: {{ $value.port }}
      protocol: {{ $value.protocol | default "TCP" | quote }}
      {{- if and $value.nodePort (not (eq $.Values.federation.service.type "ClusterIP")) }}
      nodePort: {{ $value.nodePort }}
      {{- end }}
    {{- end }}
  selector:
    {{- include "common.labels.matchLabels" . | nindent 4 }}
...
{{- end }}
