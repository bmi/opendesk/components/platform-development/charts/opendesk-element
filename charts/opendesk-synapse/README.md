<!--
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# opendesk-synapse

Deploy Matrix/Synapse.

## Installing the Chart

To install the chart with the release name `my-release`, you have two options:

### Install via Repository
```console
helm repo add opendesk-element https://gitlab.opencode.de/api/v4/projects/2291/packages/helm/stable
helm install my-release --version 6.1.0 opendesk-element/opendesk-synapse
```

### Install via OCI Registry
```console
helm repo add opendesk-element oci://registry.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element
helm install my-release --version 6.1.0 opendesk-element/opendesk-synapse
```

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| oci://registry.opencode.de/bmi/opendesk/components/external/charts/bitnami-charts | common | ^2.x.x |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Affinity for pod assignment Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity Note: podAffinityPreset, podAntiAffinityPreset, and  nodeAffinityPreset will be ignored when it's set |
| commonAnnotations | object | `{}` | Additional custom annotations to add to all deployed objects. |
| commonLabels | object | `{}` | Additional custom labels to add to all deployed objects. |
| configuration.additionalConfiguration | object | `{}` | Additional configuration to place into homeserver.yaml |
| configuration.database.channelBinding | string | `"prefer"` | This option controls the client's use of channel binding. |
| configuration.database.clientEncoding | string | `"auto"` | This sets the client_encoding configuration parameter for this connection. |
| configuration.database.connectTimeout | int | `10` | Maximum time to wait while connecting, in seconds. |
| configuration.database.connectionPoolMax | string | `"5"` | The minimum number of connections in pool. |
| configuration.database.connectionPoolMin | string | `"3"` | The minimum number of connections in pool. |
| configuration.database.gssencmode | string | `"prefer"` | This option determines whether or with what priority a secure GSS TCP/IP connection will be negotiated with the server. |
| configuration.database.host | string | `"postgresql"` | External database host address. |
| configuration.database.keepalives | int | `1` | Controls whether client-side TCP keepalives are used. |
| configuration.database.keepalivesCount | int | `3` | Controls the number of TCP keepalives that can be lost before the client's connection to the server is considered dead |
| configuration.database.keepalivesIdle | int | `10` | Controls the number of seconds of inactivity after which TCP should send a keepalive message to the server. |
| configuration.database.keepalivesInterval | int | `10` | Controls the number of seconds after which a TCP keepalive message that is not acknowledged by the server should be retransmitted. |
| configuration.database.name | string | `"matrix"` | External database name. |
| configuration.database.password.existingSecret.key | string | `""` | Key in secret containing the password. |
| configuration.database.password.existingSecret.name | string | `""` | Secret name containing password (higher precedence than plain value). |
| configuration.database.password.value | string | `""` | External database password key as plain value. |
| configuration.database.port | int | `5432` | External database port. |
| configuration.database.replication | bool | `false` | This option determines whether the connection should use the replication protocol instead of the normal protocol. |
| configuration.database.requireAuth | list | `[]` | Specifies the authentication method that the client requires from the server. |
| configuration.database.sslMinProtocolVersion | string | `"TLSv1.2"` | This parameter specifies the minimum SSL/TLS protocol version to allow for the connection |
| configuration.database.sslcompression | int | `0` | If set to 1, data sent over SSL connections will be compressed. |
| configuration.database.sslmode | string | `"prefer"` | This option determines whether or with what priority a secure SSL TCP/IP connection will be negotiated with the server. |
| configuration.database.user | string | `"matrix_user"` | External database user. |
| configuration.homeserver.appServiceConfigs | list | `[]` | A list of app service registration files that should be enabled in the homeserver Ref.: https://matrix-org.github.io/synapse/latest/application_services.html |
| configuration.homeserver.baseUrl | string | `""` | URL of synapse deployment. As default the url of synapse will be used. |
| configuration.homeserver.guestModule.configuration | object | `{"display_name_suffix":" (Gast)"}` | Configurations for the guest module. https://github.com/nordeck/element-web-modules/tree/main/packages/synapse-guest-module#module-configuration |
| configuration.homeserver.guestModule.enabled | bool | `false` | Enable the guest module. |
| configuration.homeserver.guestModule.image.imagePullPolicy | string | `"IfNotPresent"` | Define an ImagePullPolicy.  Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy  "IfNotPresent" => The image is pulled only if it is not already present locally. "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to             resolve the name to an image digest. If the kubelet has a container image with that exact digest             cached locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the             resolved digest, and uses that image to launch the container. "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the            kubelet attempts to start the container; otherwise, startup fails  |
| configuration.homeserver.guestModule.image.registry | string | `"ghcr.io"` | Container registry address. This setting has higher precedence than global.registry. |
| configuration.homeserver.guestModule.image.repository | string | `"nordeck/synapse-guest-module"` | Container repository string. |
| configuration.homeserver.guestModule.image.tag | string | `"latest"` | Define image tag. |
| configuration.homeserver.macaroonSecretKey | string | `""` | Macaroon Secret Key. Ref.: https://matrix-org.github.io/synapse/latest/usage/configuration/config_documentation.html |
| configuration.homeserver.oidc.clientId | string | `"matrix"` | OAUTH2 client id to use. |
| configuration.homeserver.oidc.clientSecret | string | `""` | OAUTH2 client secret to use. |
| configuration.homeserver.oidc.enabled | bool | `true` | Enable keycloak OIDC client. |
| configuration.homeserver.oidc.issuer | string | `""` | The OIDC issuer. Used to validate tokens and (if discovery is enabled) to discover the provider's endpoints. |
| configuration.homeserver.oidc.matrixIdLocalpart | string | `"opendesk_useruuid"` | Which OIDC `user` claim to use as localpart for the MatrixIDs. The localpart is only set on account creation. Afterwards the user is identified by the `subject_template` which the LDAP entryUUID is used for. So changes to the username will not affect the account, as the localpart of the MatrixID stays as it is (or was). |
| configuration.homeserver.oidc.scopes | list | `["openid","profile","email"]` | List of scopes to request. This should normally include the "openid" scope. |
| configuration.homeserver.presence.enabled | bool | `false` |  |
| configuration.homeserver.profile.allowUsersToUpdateDisplayname | bool | `false` | Set to true if you want to allow users to update their own display name. |
| configuration.homeserver.registrationSharedSecret | string | `""` | Registration Shared Secret. Ref.: https://matrix-org.github.io/synapse/latest/usage/configuration/config_documentation.html |
| configuration.homeserver.serverName | string | `""` | Name of synapse server. As default the name of synapse will be used. |
| configuration.homeserver.signingKey | string | `""` | Signing Key. Ref.: https://matrix-org.github.io/synapse/latest/usage/configuration/config_documentation.html |
| configuration.homeserver.smtp.enabled | bool | `true` | Enable sending of mails for Synapse. If it is not enabled the user still has the option to set notification preferences in the Element frontend, but notifications will not be sent. Ensure you set the other required properties as well. |
| configuration.homeserver.smtp.host | string | `""` | The SMTP relay host. |
| configuration.homeserver.smtp.password | string | `""` | The password for SMTP authentication at the relay host. |
| configuration.homeserver.smtp.port | int | `587` | The SMTP relay host's port. |
| configuration.homeserver.smtp.senderAddress | string | `nil` | The mail address notifications are sent from. |
| configuration.homeserver.smtp.starttls | bool | `true` | Use StartTLS for traffic encryption. |
| configuration.homeserver.smtp.tls | bool | `false` | Require SSL/TLS encryption for connection. |
| configuration.homeserver.smtp.username | string | `""` | The username for SMTP authentication at the relay host. |
| configuration.homeserver.turn.allowGuests | bool | `true` | Whether guests should be allowed to use the TURN server. This defaults to true, otherwise VoIP will be unreliable for guests |
| configuration.homeserver.turn.credentials | object | `{"password":"","username":""}` | The Username and password if the TURN server needs them and does not use a token. |
| configuration.homeserver.turn.enabled | bool | `true` | Enable custom turn server. |
| configuration.homeserver.turn.servers | list | `[{"port":3478,"server":"turn.matrix.org","transport":"udp"},{"port":3478,"server":"turn.matrix.org","transport":"tcp"}]` | The public URIs of the TURN server to give to clients |
| configuration.homeserver.turn.sharedSecret | string | `""` | The shared secret used to compute passwords for the TURN server. |
| configuration.homeserver.turn.userLifetime | string | `"1h"` | How long generated TURN credentials last. |
| containerSecurityContext.allowPrivilegeEscalation | bool | `false` | Enable container privileged escalation. |
| containerSecurityContext.capabilities | object | `{"drop":["ALL"]}` | Additional capabilities. |
| containerSecurityContext.enabled | bool | `true` | Enable security context. |
| containerSecurityContext.privileged | bool | `false` | Run container in privileged mode. |
| containerSecurityContext.readOnlyRootFilesystem | bool | `true` | Mounts the container's root filesystem as read-only. |
| containerSecurityContext.runAsNonRoot | bool | `true` | Run container as user. |
| containerSecurityContext.runAsUser | int | `10991` | Process user id. |
| containerSecurityContext.seccompProfile.type | string | `"RuntimeDefault"` | Disallow custom Seccomp profile by setting it to RuntimeDefault. |
| extraEnvVars | list | `[]` | Array with extra environment variables to add to containers.  extraEnvVars:   - name: FOO     value: "bar"  |
| extraVolumeMounts | list | `[]` | Optionally specify extra list of additional volumeMounts. |
| extraVolumes | list | `[]` | Optionally specify extra list of additional volumes |
| federation.domainAllowList | list | `[]` | Allow list for domains of Matrix homeservers to federate with.  Ref.: https://element-hq.github.io/synapse/latest/usage/configuration/config_documentation.html#federation_domain_whitelist |
| federation.enabled | bool | `true` | Enable matrix federation service and ingress. |
| federation.ingress.annotations | object | `{}` | Define custom ingress annotations.  annotations:   nginx.ingress.kubernetes.io/rewrite-target: /  |
| federation.ingress.enabled | bool | `true` | Enable creation of Ingress. |
| federation.ingress.host | string | `""` | Define the Fully Qualified Domain Name (FQDN) where UCS Portal should be reachable. This setting has higher precedence than global.registry. |
| federation.ingress.ingressClassName | string | `"nginx"` | The Ingress controller class name. |
| federation.ingress.pathType | string | `"Prefix"` | Each path in an Ingress is required to have a corresponding path type. Paths that do not include an explicit pathType will fail validation. There are three supported path types:  "ImplementationSpecific" => With this path type, matching is up to the IngressClass. Implementations can treat                             this as a separate pathType or treat it identically to Prefix or Exact path types. "Exact" => Matches the URL path exactly and with case sensitivity. "Prefix" => Matches based on a URL path prefix split by /.  Ref.: https://kubernetes.io/docs/concepts/services-networking/ingress/#path-types  |
| federation.ingress.tls | object | `{"enabled":true,"secretName":""}` | Secure an Ingress by specifying a Secret that contains a TLS private key and certificate.  Ref.: https://kubernetes.io/docs/concepts/services-networking/ingress/#tls  |
| federation.ingress.tls.enabled | bool | `true` | Enable TLS/SSL/HTTPS for Ingress.  |
| federation.ingress.tls.secretName | string | `""` | The name of the kubernetes secret which contains a TLS private key and certificate. Hint: This secret is not created by this chart and must be provided. |
| federation.service.annotations | object | `{}` | Additional custom annotations |
| federation.service.ports.federation.containerPort | int | `8448` | Internal port. |
| federation.service.ports.federation.port | int | `8448` | Accessible port. |
| federation.service.ports.federation.protocol | string | `"TCP"` | Service protocol. |
| federation.service.type | string | `"ClusterIP"` | Choose the kind of Service, one of "ClusterIP", "NodePort" or "LoadBalancer". |
| fullnameOverride | string | `""` | Provide a name to substitute for the full names of resources. |
| global.domain | string | `"example.com"` | The Top-Level-Domain (TLD) name which is used in f.e. in Ingress component. |
| global.hosts.element | string | `"element"` | Subdomain for element, results in "https://{{ element }}.{{ domain }}". |
| global.hosts.synapse | string | `"synapse"` | Subdomain for synapse, results in "https://{{ synapse }}.{{ domain }}". |
| global.imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| global.registry | string | `"docker.io"` | Container registry address. |
| image.imagePullPolicy | string | `"IfNotPresent"` | Define an ImagePullPolicy.  Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy  "IfNotPresent" => The image is pulled only if it is not already present locally. "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to             resolve the name to an image digest. If the kubelet has a container image with that exact digest cached             locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the resolved             digest, and uses that image to launch the container. "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the            kubelet attempts to start the container; otherwise, startup fails  |
| image.registry | string | `""` | Container registry address. This setting has higher precedence than global.registry. |
| image.repository | string | `"matrixdotorg/synapse"` | Container repository string. |
| image.tag | string | `"v1.116.0@sha256:2d5c371f32694401734a8a7dccffaaf89aadee645df4f23e48bab48223cba5ea"` | Define image tag. |
| imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| lifecycleHooks | object | `{}` | Lifecycle to automate configuration before or after startup |
| livenessProbe.enabled | bool | `true` | Enables kubernetes LivenessProbe. |
| livenessProbe.failureThreshold | int | `8` | Number of failed executions until container is terminated. |
| livenessProbe.initialDelaySeconds | int | `1` | Delay after container start until LivenessProbe is executed. |
| livenessProbe.periodSeconds | int | `6` | Time between probe executions. |
| livenessProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| livenessProbe.timeoutSeconds | int | `2` | Timeout for command return. |
| nameOverride | string | `""` | String to partially override release name. |
| nodeSelector | object | `{}` | Node labels for pod assignment Ref: https://kubernetes.io/docs/user-guide/node-selection/ |
| persistence.accessModes | list | `["ReadWriteOnce"]` | The volume access modes, some of "ReadWriteOnce", "ReadOnlyMany", "ReadWriteMany", "ReadWriteOncePod".  "ReadWriteOnce" => The volume can be mounted as read-write by a single node. ReadWriteOnce access mode still can                    allow multiple pods to access the volume when the pods are running on the same node. "ReadOnlyMany" => The volume can be mounted as read-only by many nodes. "ReadWriteMany" => The volume can be mounted as read-write by many nodes. "ReadWriteOncePod" => The volume can be mounted as read-write by a single Pod. Use ReadWriteOncePod access mode if                       you want to ensure that only one pod across whole cluster can read that PVC or write to it.  |
| persistence.annotations | object | `{}` | Annotations for the PVC. |
| persistence.dataSource | object | `{}` | Custom PVC data source. |
| persistence.enabled | bool | `true` | Enable data persistence (true) or use temporary storage (false). |
| persistence.existingClaim | string | `""` | Use an already existing claim. |
| persistence.labels | object | `{}` | Labels for the PVC. |
| persistence.selector | object | `{}` | Selector to match an existing Persistent Volume (this value is evaluated as a template)  selector:   matchLabels:     app: my-app  |
| persistence.size | string | `"1Gi"` | The volume size with unit. |
| persistence.storageClass | string | `""` | The (storage) class of PV. |
| persistence.temporaryDirectory.size | string | `"50Mi"` | Size limit of emptyDir volume. |
| podAnnotations | object | `{}` | Pod Annotations. Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/ |
| podLabels | object | `{}` | Pod Labels. Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/ |
| podSecurityContext.enabled | bool | `true` | Enable security context. |
| podSecurityContext.fsGroup | int | `10991` | If specified, all processes of the container are also part of the supplementary group |
| readinessProbe.enabled | bool | `true` | Enables kubernetes ReadinessProbe. |
| readinessProbe.failureThreshold | int | `8` | Number of failed executions until container is terminated. |
| readinessProbe.initialDelaySeconds | int | `1` | Delay after container start until ReadinessProbe is executed. |
| readinessProbe.periodSeconds | int | `2` | Time between probe executions. |
| readinessProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| readinessProbe.timeoutSeconds | int | `2` | Timeout for command return. |
| replicaCount | int | `1` | Set the amount of replicas of statefulset. |
| resources.limits.cpu | int | `4` | The max amount of CPUs to consume. |
| resources.limits.memory | string | `"4Gi"` | The max amount of RAM to consume. |
| resources.requests.cpu | int | `1` | The amount of CPUs which has to be available on the scheduled node. |
| resources.requests.memory | string | `"2Gi"` | The amount of RAM which has to be available on the scheduled node. |
| service.annotations | object | `{}` | Additional custom annotations |
| service.clusterIP | string | `"None"` | Set the clusterIP to None to create a Headless Service. |
| service.enabled | bool | `true` | Enable kubernetes service creation. |
| service.ports.synapse-health.containerPort | int | `8080` | Internal port. |
| service.ports.synapse-health.port | int | `8080` | Accessible port. |
| service.ports.synapse-health.protocol | string | `"TCP"` | Service protocol. |
| service.ports.synapse-http.containerPort | int | `8008` | Internal port. |
| service.ports.synapse-http.port | int | `8008` | Accessible port. |
| service.ports.synapse-http.protocol | string | `"TCP"` | Service protocol. |
| service.ports.synapse-metrics.containerPort | int | `9001` | Internal port. |
| service.ports.synapse-metrics.port | int | `9001` | Accessible port. |
| service.ports.synapse-metrics.protocol | string | `"TCP"` | Service protocol. |
| service.ports.synapse-repl.containerPort | int | `9093` | Internal port. |
| service.ports.synapse-repl.port | int | `9093` | Accessible port. |
| service.ports.synapse-repl.protocol | string | `"TCP"` | Service protocol. |
| service.type | string | `"ClusterIP"` | Choose the kind of Service, one of "ClusterIP", "NodePort" or "LoadBalancer". |
| serviceAccount.annotations | object | `{}` | Additional custom annotations for the ServiceAccount. |
| serviceAccount.automountServiceAccountToken | bool | `false` | Allows auto mount of ServiceAccountToken on the serviceAccount created. Can be set to false if pods using this serviceAccount do not need to use K8s API. |
| serviceAccount.create | bool | `true` | Enable creation of ServiceAccount for pod. |
| serviceAccount.labels | object | `{}` | Additional custom labels for the ServiceAccount. |
| startupProbe.enabled | bool | `true` | Enables kubernetes StartupProbe. |
| startupProbe.failureThreshold | int | `10` | Number of failed executions until container is terminated. |
| startupProbe.initialDelaySeconds | int | `5` | Delay after container start until StartupProbe is executed. |
| startupProbe.periodSeconds | int | `2` | Time between probe executions. |
| startupProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| startupProbe.timeoutSeconds | int | `1` | Timeout for command return. |
| terminationGracePeriodSeconds | int | `30` | In seconds, time the given to the pod needs to terminate gracefully. Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods |
| tolerations | list | `[]` | Tolerations for pod assignment Ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/ |
| topologySpreadConstraints | list | `[]` | Topology spread constraints rely on node labels to identify the topology domain(s) that each Node is in Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-topology-spread-constraints/  topologySpreadConstraints:   - maxSkew: 1     topologyKey: failure-domain.beta.kubernetes.io/zone     whenUnsatisfiable: DoNotSchedule |
| updateStrategy.type | string | `"RollingUpdate"` | Set to Recreate if you use persistent volume that cannot be mounted by more than one pods to make sure the pods is destroyed first. |

## Uninstalling the Chart

To install the release with name `my-release`:

```bash
helm uninstall my-release
```

## Signing

Helm charts are signed with helm native signing method.

You can verify the chart against [the public GPG key](../../files/gpg-pubkeys/opendesk.gpg).

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright (C) 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
