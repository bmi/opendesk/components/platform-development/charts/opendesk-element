<!--
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-License-Identifier: Apache-2.0
-->
# opendesk-synapse-groupsync

Deploy Matrix/Synapse Groupsync.

## Installing the Chart

To install the chart with the release name `my-release`, you have two options:

### Install via Repository
```console
helm repo add opendesk-element https://gitlab.opencode.de/api/v4/projects/2291/packages/helm/stable
helm install my-release --version 6.1.0 opendesk-element/opendesk-synapse-groupsync
```

### Install via OCI Registry
```console
helm repo add opendesk-element oci://registry.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element
helm install my-release --version 6.1.0 opendesk-element/opendesk-synapse-groupsync
```

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| oci://registry.opencode.de/bmi/opendesk/components/external/charts/bitnami-charts | common | ^2.x.x |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Affinity for pod assignment Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity Note: podAffinityPreset, podAntiAffinityPreset, and  nodeAffinityPreset will be ignored when it's set |
| commonAnnotations | object | `{}` | Additional custom annotations to add to all deployed objects. |
| commonLabels | object | `{}` | Additional custom labels to add to all deployed objects. |
| configuration.asToken | string | `""` | Application Service Token for groupsync |
| configuration.dryRun | bool | `false` | Set to true to only show what would be done instead of actually changing the database |
| configuration.groupSyncService | string | `"opendesk-synapse-groupsync"` | Name of group sync service (default opendesk-synapse-groupsync) |
| configuration.homeserverName | string | `""` | Name of homeserver |
| configuration.hsToken | string | `""` | Appservice Homeserver Token for groupsync |
| configuration.id | string | `"gps"` | Appservice id for groupsync |
| configuration.ldap | object | `{"attributes":{"name":"description","uid":"cn"},"base":"dc=example,dc=localhost","bind_dn":"cn=readonly,dc=example,dc=localhost","bind_password":"","check_interval_seconds":60,"type":"mapped-ldap","uri":"ldaps://ldap.example.localhost:636"}` | LDAP config |
| configuration.ldap.attributes.name | string | `"description"` | Use LDAP description as name. |
| configuration.ldap.attributes.uid | string | `"cn"` | Use LDAP cn as UID. |
| configuration.ldap.base | string | `"dc=example,dc=localhost"` | LDAP base. |
| configuration.ldap.bind_dn | string | `"cn=readonly,dc=example,dc=localhost"` | LDAP DN. |
| configuration.ldap.bind_password | string | `""` | LDAP password. |
| configuration.ldap.check_interval_seconds | int | `60` | In which intervals to check LDAP. |
| configuration.ldap.type | string | `"mapped-ldap"` | Type is mapped-ldap |
| configuration.ldap.uri | string | `"ldaps://ldap.example.localhost:636"` | LDAP URI. |
| configuration.provisionerDefaultRooms | string | `nil` | Define default rooms. |
| configuration.provisionerInviteToPublicRooms | bool | `true` | Whether if provisioner should invite to public rooms. |
| configuration.registrationSecret | string | `"opendesk-synapse"` | Secret used by synapse for registered components |
| configuration.registrationSecretKey | string | `"app-service-gps.yaml"` | Key where Group Sync registration is stored |
| configuration.registrationSharedSecret | string | `""` | Registration Shared Secret value. Ref.: https://matrix-org.github.io/synapse/latest/usage/configuration/config_documentation.html |
| configuration.runOnce | bool | `false` | Set to true to only run once (e.g. for debugging) |
| configuration.spaces | string | `nil` | Space mapping Ref.: https://ems-docs.element.io/books/element-on-premise-documentation-lts-2310/page/setting-up-group-sync-with-the-installer |
| configuration.synapseWebService | string | `"opendesk-synapse-web"` | Service name of synapse web component |
| configuration.username | string | `"groupsyncbot"` | matrix username of groupsyncbot |
| containerSecurityContext.allowPrivilegeEscalation | bool | `false` | Enable container privileged escalation. |
| containerSecurityContext.capabilities | object | `{"drop":["ALL"]}` | Additional capabilities. |
| containerSecurityContext.enabled | bool | `true` | Enable security context. |
| containerSecurityContext.privileged | bool | `false` | Run container in privileged mode. |
| containerSecurityContext.readOnlyRootFilesystem | bool | `true` | Mounts the container's root filesystem as read-only. |
| containerSecurityContext.runAsNonRoot | bool | `true` | Run container as user. |
| containerSecurityContext.runAsUser | int | `10010` | Process user id. |
| containerSecurityContext.seccompProfile.type | string | `"RuntimeDefault"` | Disallow custom Seccomp profile by setting it to RuntimeDefault. |
| extraEnvVars | list | `[]` | Array with extra environment variables to add to containers.  extraEnvVars:   - name: FOO     value: "bar"  |
| extraVolumeMounts | list | `[]` | Optionally specify extra list of additional volumeMounts. |
| extraVolumes | list | `[]` | Optionally specify extra list of additional volumes |
| fullnameOverride | string | `""` | Provide a name to substitute for the full names of resources. |
| global.domain | string | `"example.com"` | The Top-Level-Domain (TLD) name which is used in f.e. in Ingress component. |
| global.hosts.element | string | `"element"` | Subdomain for element, results in "https://{{ element }}.{{ domain }}". |
| global.hosts.synapse | string | `"synapse"` | Subdomain for synapse, results in "https://{{ synapse }}.{{ domain }}". |
| global.imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| global.registry | string | `"docker.io"` | Container registry address. |
| image.imagePullPolicy | string | `"IfNotPresent"` | Define an ImagePullPolicy.  Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy  "IfNotPresent" => The image is pulled only if it is not already present locally. "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to             resolve the name to an image digest. If the kubelet has a container image with that exact digest cached             locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the resolved             digest, and uses that image to launch the container. "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the            kubelet attempts to start the container; otherwise, startup fails  |
| image.registry | string | `"registry.opencode.de"` | Container registry address. This setting has higher precedence than global.registry. |
| image.repository | string | `"zendis/opendesk-enterprise/components/supplier/element/images-mirror/groupsync"` | Container repository string. |
| image.tag | string | `"v0.14.0@sha256:a8cee92b9035d8cc80cc13194e4e0118c7dfbfcbc4c0ee5ac173582d0cd55846"` | Define image tag. |
| imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| lifecycleHooks | object | `{}` | Lifecycle to automate configuration before or after startup |
| livenessProbe.enabled | bool | `true` | Enables kubernetes LivenessProbe. |
| livenessProbe.failureThreshold | int | `8` | Number of failed executions until container is terminated. |
| livenessProbe.initialDelaySeconds | int | `1` | Delay after container start until LivenessProbe is executed. |
| livenessProbe.periodSeconds | int | `6` | Time between probe executions. |
| livenessProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| livenessProbe.timeoutSeconds | int | `2` | Timeout for command return. |
| nameOverride | string | `""` | String to partially override release name. |
| nodeSelector | object | `{}` | Node labels for pod assignment Ref: https://kubernetes.io/docs/user-guide/node-selection/ |
| podAnnotations | object | `{}` | Pod Annotations. Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/ |
| podLabels | object | `{}` | Pod Labels. Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/ |
| podSecurityContext.enabled | bool | `true` | Enable security context. |
| podSecurityContext.fsGroup | int | `10010` | If specified, all processes of the container are also part of the supplementary group |
| readinessProbe.enabled | bool | `true` | Enables kubernetes ReadinessProbe. |
| readinessProbe.failureThreshold | int | `8` | Number of failed executions until container is terminated. |
| readinessProbe.initialDelaySeconds | int | `1` | Delay after container start until ReadinessProbe is executed. |
| readinessProbe.periodSeconds | int | `2` | Time between probe executions. |
| readinessProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| readinessProbe.timeoutSeconds | int | `2` | Timeout for command return. |
| replicaCount | int | `1` | Set the amount of replicas of statefulset. |
| resources.limits.cpu | int | `1` | The max amount of CPUs to consume. |
| resources.limits.memory | string | `"1000Mi"` | The max amount of RAM to consume. |
| resources.requests.cpu | string | `"100m"` | The amount of CPUs which has to be available on the scheduled node. |
| resources.requests.memory | string | `"150Mi"` | The amount of RAM which has to be available on the scheduled node. |
| service.annotations | object | `{}` | Additional custom annotations |
| service.clusterIP | string | `"None"` | Set the clusterIP to None to create a Headless Service. |
| service.enabled | bool | `true` | Enable kubernetes service creation. |
| service.ports.appservice.containerPort | int | `10010` | Internal port. |
| service.ports.appservice.port | int | `10010` | Accessible port. |
| service.ports.appservice.protocol | string | `"TCP"` | Service protocol. |
| service.ports.metrics.containerPort | int | `3131` | Internal port. |
| service.ports.metrics.port | int | `3131` | Accessible port. |
| service.ports.metrics.protocol | string | `"TCP"` | Service protocol. |
| service.type | string | `"ClusterIP"` | Choose the kind of Service, one of "ClusterIP", "NodePort" or "LoadBalancer". |
| serviceAccount.annotations | object | `{}` | Additional custom annotations for the ServiceAccount. |
| serviceAccount.automountServiceAccountToken | bool | `false` | Allows auto mount of ServiceAccountToken on the serviceAccount created. Can be set to false if pods using this serviceAccount do not need to use K8s API. |
| serviceAccount.create | bool | `true` | Enable creation of ServiceAccount for pod. |
| serviceAccount.labels | object | `{}` | Additional custom labels for the ServiceAccount. |
| startupProbe.enabled | bool | `true` | Enables kubernetes StartupProbe. |
| startupProbe.failureThreshold | int | `10` | Number of failed executions until container is terminated. |
| startupProbe.initialDelaySeconds | int | `5` | Delay after container start until StartupProbe is executed. |
| startupProbe.periodSeconds | int | `2` | Time between probe executions. |
| startupProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| startupProbe.timeoutSeconds | int | `1` | Timeout for command return. |
| terminationGracePeriodSeconds | int | `30` | In seconds, time the given to the pod needs to terminate gracefully. Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods |
| tolerations | list | `[]` | Tolerations for pod assignment Ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/ |
| topologySpreadConstraints | list | `[]` | Topology spread constraints rely on node labels to identify the topology domain(s) that each Node is in Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-topology-spread-constraints/  topologySpreadConstraints:   - maxSkew: 1     topologyKey: failure-domain.beta.kubernetes.io/zone     whenUnsatisfiable: DoNotSchedule |
| updateStrategy.type | string | `"RollingUpdate"` | Set to Recreate if you use persistent volume that cannot be mounted by more than one pods to make sure the pods is destroyed first. |

## Uninstalling the Chart

To install the release with name `my-release`:

```bash
helm uninstall my-release
```

## Signing

Helm charts are signed with helm native signing method.

You can verify the chart against [the public GPG key](../../files/gpg-pubkeys/opendesk.gpg).

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright (C) 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
