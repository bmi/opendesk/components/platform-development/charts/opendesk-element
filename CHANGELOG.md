# [6.1.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v6.0.2...v6.1.0) (2025-02-24)


### Features

* Support for `domainAllowList` on federation configuration ([4563fcd](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/4563fcdf707df3f9331becf0e6546d74678e3ba5))

## [6.0.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v6.0.1...v6.0.2) (2025-01-19)


### Bug Fixes

* **opendesk-synapse:** Streamline indent in `secret.yaml` ([629ec0c](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/629ec0c682968e13be70e2b0f6234bd7ecad4291))

## [6.0.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v6.0.0...v6.0.1) (2024-12-20)


### Bug Fixes

* Remove hardcoded `Collaboration` from title ([62d2ab9](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/62d2ab996f2ccad59eeb3f7c21f5fd3ad032430d))

# [6.0.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v5.0.1...v6.0.0) (2024-12-20)


### Features

* Support for explicitly set HTLM title ([458f8dc](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/458f8dc1e8a641618a99be36499befe34c5dd2bd))


### BREAKING CHANGES

* Rename `opendesk-element-web` to `opendesk-element` and append `-umbrella` to the Umbrella chart

## [5.0.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v5.0.0...v5.0.1) (2024-11-25)


### Bug Fixes

* **adminbot-web:** Allow from IPv6 as well. ([c69e5b6](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/c69e5b63bee7f1c0edbc2a8c8e484d5597f5cbe0))
* **element:** Update image versions. ([bb80681](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/bb80681a1bf53150936a6e89933ddb31e0a83d5c))
* **synapse-admin:** Add cronjob for admin sync. ([525f326](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/525f3265e76fd0812a598a8b093a5414873277d3))
* **synapse-groupsync:** Add group-sync options. ([e92a66f](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/e92a66f1e07785d40808fa6214ce7c01f50e5f9b))

# [5.0.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v4.0.0...v5.0.0) (2024-11-18)


### Bug Fixes

* **opendesk-synapse:** Add database settings ([035ea42](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/035ea42d8fb6c4d8b067946020fedad0d58c1345))


### BREAKING CHANGES

* **opendesk-synapse:** Refactor configuration.database.password into external secret reference

# [4.0.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v3.4.1...v4.0.0) (2024-11-04)


### Features

* **element:** Add Enterprise charts and umbrella chart. ([aa6f94e](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/aa6f94e6c7a4b8ad6db793894337f7d101f3de7c))


### BREAKING CHANGES

* **element:** Use `global.registry` instead of `global.imageRegistry` in each chart.

## [3.4.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v3.4.0...v3.4.1) (2024-09-19)


### Bug Fixes

* **configmap:** Handling empty `appServiceConfigs`. ([9ae9bd7](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/9ae9bd744dda22cfc9b994ce3143501110a2aba0))

# [3.4.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v3.3.2...v3.4.0) (2024-08-28)


### Bug Fixes

* **element:** Explicitly set the default federation setting for public rooms to "do not federate". ([a131e5d](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/a131e5d8a30bbc4d8866eea553850cb7db187101))


### Features

* **profile:** Support for templating the OIDC claim to be used for MatrixID localpart and feature toggle for allowing users changing their display name. ([56b47fb](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/56b47fb9f72dc5a8fc696e91b9f9748bf69b4411))

## [3.3.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v3.3.1...v3.3.2) (2024-08-19)


### Bug Fixes

* **smtp:** Settings for TLS and Auth. ([48d7be9](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/48d7be9f10736796c3bc7751c0c3adaa54c58e76))

## [3.3.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v3.3.0...v3.3.1) (2024-08-08)


### Bug Fixes

* **opendesk-synapse:** Template tls settings for smtp ([b3fe9e9](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/b3fe9e9ddc8bbd2aaaf73dd1b0bea681d791841f))

# [3.3.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v3.2.0...v3.3.0) (2024-07-30)


### Features

* **element:** Featuretoggle for presence propagation; Disable the option for users to set their display name (as it is taken from the IAM). ([1022bcc](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/1022bccfc28d38141732a3ec3535bbdc18c5c895))

# [3.2.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v3.1.0...v3.2.0) (2024-06-28)


### Features

* **synapse:** Mail notifications. ([69cd971](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/69cd971b71dae7d1ad4a4a25b6f5a8df6028b4a3))

# [3.1.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v3.0.0...v3.1.0) (2024-06-18)


### Features

* **opendesk-synapse-create-account:** basic ArgoCD support ([a62e865](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/a62e8659231721484a2896c92ef15a2637f11e0f))

# [3.0.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v2.7.1...v3.0.0) (2024-06-02)


### Features

* **opendesk-synapse:** Add federation via dedicated ingress ([0050df2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/0050df203067f1e06fdfed3829133346151cb855))


### BREAKING CHANGES

* **opendesk-synapse:** Remove tls secret from synapse container

## [2.7.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v2.7.0...v2.7.1) (2024-05-05)


### Bug Fixes

* Add support for value "global.clusterDomain" ([3094065](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/3094065c8eeced2f6ddcbc14b36dc326a940e863))

# [2.7.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v2.6.7...v2.7.0) (2024-05-05)


### Features

* **opendesk-synapse:** Add NodePort service for synapse to allow federation ([09123e2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/09123e2da7453961145451bd413b66e11938b27b))

## [2.6.7](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v2.6.6...v2.6.7) (2024-02-28)


### Bug Fixes

* **element:** Add config section for widget toggles module ([710011c](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/710011c0a01c5b4f086ae00a4843644a56b56e98))

## [2.6.6](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v2.6.5...v2.6.6) (2024-01-22)


### Bug Fixes

* **opendesk-synapse:** Add missing imagePullPolicy for initContainer template ([a0d1c7a](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/a0d1c7af964072c84583f29bae1ec5273e1c149a))

## [2.6.5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v2.6.4...v2.6.5) (2024-01-09)


### Bug Fixes

* **element:** Allow toggle for e2ee ([2f54dfd](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/2f54dfdfcfd719bed5fa82c234df0821ab89c13f))

## [2.6.4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v2.6.3...v2.6.4) (2023-12-21)


### Bug Fixes

* Add GPG key, update README.md ([067a1de](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/067a1def11f2739f604a598a1ad19663aca3c712))

## [2.6.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/compare/v2.6.2...v2.6.3) (2023-12-20)


### Bug Fixes

* **ci:** Move repo to Open CoDE ([b0fd381](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element/commit/b0fd381cdc7ff417d36dcb79d9a37fe9d98c77a1))

## [2.6.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/compare/v2.6.1...v2.6.2) (2023-12-15)


### Bug Fixes

* **opendesk-oidc:** Update for streamlined Keycloak Config ([d2a210b](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/d2a210bef4176877353a891f748823b95b2a05cd))

## [2.6.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/compare/v2.6.0...v2.6.1) (2023-12-13)


### Bug Fixes

* **opendesk-synapse-web:** Fix fd limit is too low on small nodes ([1472a69](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/1472a69b883c42c567fd641c0e4525dba0fc311f))

# [2.6.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/compare/v2.5.1...v2.6.0) (2023-12-08)


### Features

* **opendesk-element:** Add translations ([ea1f37c](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/ea1f37cc32cf2f396ec5aacc5d0264d9f03aea81))

## [2.5.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/compare/v2.5.0...v2.5.1) (2023-11-22)


### Bug Fixes

* **opendesk-element:** Use "| quote" wherever possible ([ff578b6](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/ff578b69e99d2eeaaa98d321e3ef853c6ca0f298))

# [2.5.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/compare/v2.4.0...v2.5.0) (2023-10-20)


### Bug Fixes

* **opendesk-element:** Only show an image in the homepage is the image is configured ([8222df4](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/8222df417a0a5358372256cfa867310a70c03383))


### Features

* **opendesk-element:** Remove the custom css file that is replaced by an Element module ([42a3608](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/42a36087d01e22e759230afd16a5561a95b7323d))

# [2.4.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/compare/v2.3.0...v2.4.0) (2023-10-19)


### Features

* **opendesk-element:** Configure feature ask to join ([786fab9](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/786fab9ca79937144899e4f892c4d1f31df5d646))

# [2.3.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/compare/v2.2.0...v2.3.0) (2023-10-17)


### Bug Fixes

* **opendesk-well-known:** Correct the Server-Server API discovery configuration ([c0dba55](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/c0dba5566b2dfb7f9fd2aa859b79df5d8294e8e4))


### Features

* **opendesk-element:** Add support for a custom home page that links to a DM with a specified user ([8c9d35c](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/8c9d35cc53e6cf50a4f28ede1596fbdc7240320f))
* **opendesk-element:** Provide custom translations for the power levels ([b53cbf6](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/b53cbf6df21d708e32758e0c9e50c86f771ba2fc))
* **opendesk-matrix-user-verification-service:** Add a chart to deploy the Matrix User Verification Service ([dae88df](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/dae88dfef1fed5493a181947fe357d86abb1cffd))
* **opendesk-synapse-create-account:** Add a chart to create admin users in synapse ([45c685d](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/45c685daefa0475681454568bc1f31f091983db5))
* **opendesk-synapse:** Support custom homeserver configurations ([7379627](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/7379627c6d76fddb026284cd30508fdbef4ee8dc))
* **opendesk-synapse:** Support the registration of application service ([6b52495](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/6b524953aa63be2952b327046b84d4cf0ec9f458))

# [2.2.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/compare/v2.1.0...v2.2.0) (2023-09-27)


### Features

* **opendesk-synapse:** Use the resources section for the guest module init container ([269cca8](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/269cca8d164eeed17f8e573065fcb624f9f04d2b))

# [2.1.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/compare/v2.0.2...v2.1.0) (2023-09-25)


### Features

* **opendesk-element:** Configure the guest module ([a3f288f](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/a3f288ff049c2aa89dcf7a52d0e7cb0c4a5f2a30))
* **opendesk-element:** Use the light theme by default ([827eb5b](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/827eb5b81d0625dc3ae4d9c03938a71a9aefb2f6))
* **opendesk-synapse:** Optionally install the guest module ([237ab26](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/237ab26e26d4a2111c313ba97a24a1cba15f8647))

## [2.0.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/compare/v2.0.1...v2.0.2) (2023-09-13)


### Bug Fixes

* **opendesk-synapse:** Always enable local password authentication ([dbde9b0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/dbde9b0e0921c23a3c54adfb56024ea67efe683e))

## [2.0.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/compare/v2.0.0...v2.0.1) (2023-09-11)


### Bug Fixes

* Fix user and fs ids in podSecurityContext ([bea9ddc](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/bea9ddca27eaf43fb9857a4b79c517f7f09c949c))

# [2.0.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/compare/v1.3.0...v2.0.0) (2023-09-11)


### Bug Fixes

* Improve default containerSecurityContext ([7d45947](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/7d45947a8cb4359323076693e07cafbc4a940622))


### BREAKING CHANGES

* Rename sovereign-workplace-* to opendesk-*

# [1.3.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/compare/v1.2.0...v1.3.0) (2023-08-29)


### Bug Fixes

* **sovereign-workplace-element:** Correctly apply the additional configuration ([33b4bc2](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/33b4bc20dfc41a674ea1bf1813d18b987cfc2f43))


### Features

* **sovereign-workplace-element:** Add more Element configurations ([51b3090](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/51b3090516c84493ab8ff938c5247fe23b43a41e))
* **sovereign-workplace-element:** Add theme support ([29415b0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/29415b09e1154a51b9ab78c1de3d2a58f009a8b2))

# [1.2.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/compare/v1.1.2...v1.2.0) (2023-08-22)


### Features

* **sovereign-workplace-well-known:** Add the option to disable the end to end encryption settings in Element ([3d45f06](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/3d45f06efcf6f6dcdd5855e6b92f8b0e9e8f30b7))

## [1.1.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/compare/v1.1.1...v1.1.2) (2023-08-13)


### Bug Fixes

* **sovereign-workplace-synapse:** Fix port in turn uri ([a4ae308](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/a4ae3088d73195524d02b3bb7aa50a1cb1584308))

## [1.1.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/compare/v1.1.0...v1.1.1) (2023-08-13)


### Bug Fixes

* **sovereign-workplace-well-known:** Remove https in jitsi domain ([7d696cd](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/7d696cd8398246abb369a9725ffb7d26742d0e5b))

# [1.1.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/compare/v1.0.0...v1.1.0) (2023-08-13)


### Features

* **sovereign-workplace-element:** Add Jitsi configuration ([02f4f49](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/02f4f495007bffe76aae9ad1070ab6e794364931))
* **sovereign-workplace-synapse:** Add OIDC and TURN configuration ([0b71d36](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/0b71d360216f7929df59fbfe76f8e4ce50f1b297))
* **sovereign-workplace-well-known:** Add Jitsi configuration ([47a0b7c](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/47a0b7c10ab2eb6ff3666b51b5d016509aa1598b))

# 1.0.0 (2023-08-10)


### Features

* Initial commit ([b65cbf6](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-element/commit/b65cbf670bde1a9a40012f096f6b4e868c1d18b7))
